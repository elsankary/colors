<?php
namespace App\Http\Controllers\Backend;

use App\Models\Admin;
use Carbon\Carbon;
use Illuminate\Http\Request;

class AdminsController extends BackendController
{
    public function __construct()
    {

    }

    public function index()
    {
        $admins = Admin::get();
        return view('backend.admins.index', compact('admins'));
    }

    public function store(Request $request)
    {
        $input = $this->getInput($request);

        $admin = new Admin($input);
        $admin->save();

        flashMessage('Admin has been added successfully.');

        return redirect()->route('backend.admins.index');
    }

    public function update($id, Request $request)
    {
        $input = $this->getInput($request);

        if (!$request->has('password')) {
            unset($input['password']);
        }

        $admin = Admin::findOrFail($id);
        $admin->fill($input)->save();

        flashMessage('Admin has been updated successfully.');

        return redirect()->route('backend.admins.index');
    }

    public function destroy($id)
    {
        Admin::destroy($id);

        flashMessage('Admin has been deleted successfully.');

        return redirect()->route('backend.admins.index');
    }

    protected function getInput(Request $request)
    {
        $input = $request->only('email', 'name', 'code', 'permission', 'phone_number');
        $input['password'] = bcrypt($request->get('password'));

        return $input;
    }
}