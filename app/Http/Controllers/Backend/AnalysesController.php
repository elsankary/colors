<?php
namespace App\Http\Controllers\Backend;

use App\Models\Analysis;
use App\Models\Doctor;
use App\Models\Patient;
use Illuminate\Http\Request;

class AnalysesController extends BackendController
{
    public function __construct()
    {
        $this->setActiveGroup('accounts');
        $this->setActivePage('patients');

        parent::__construct();
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'pdf' => 'mimes:pdf,jpg,jpeg,bmp,png',
        ]);

        $input = $this->getInput($request);

        $analysis = new Analysis($input);
        $analysis->save();

        if ($request->hasFile('pdf')) {
            $analysis->addMedia($request->file('pdf'))->toMediaLibrary();
        }

        flashMessage('Analysis has been added successfully.');

        return redirect()->route('backend.patients.show', $analysis->patient_id);
    }

    public function update($id, Request $request)
    {
        $this->validate($request, [
            'pdf' => 'mimes:pdf,jpg,jpeg,bmp,png',
        ]);

        $input = $this->getInput($request);

        $analysis = Analysis::findOrFail($id);
        $analysis->fill($input)->save();

        if ($request->hasFile('pdf')) {
            $analysis->clearMediaCollection();
            $analysis->addMedia($request->file('pdf'))->toMediaLibrary();
        }

        flashMessage('Analysis has been updated successfully.');

        return redirect()->route('backend.patients.show', $analysis->patient_id);
    }

    public function destroy($id)
    {
        $analysis = Analysis::findOrFail($id);
        $analysis->delete();

        flashMessage('Analysis has been deleted successfully.');

        return redirect()->route('backend.patients.show', $analysis->patient_id);
    }

    public function addFile(Request $request)
    {
        $this->validate($request, [
            'pdf' => 'required|mimes:pdf,jpg,jpeg,bmp,png',
        ]);
        $analysis = Analysis::findOrFail($request->get('analysis_id'));

        if ($request->hasFile('pdf')) {
            $analysis->clearMediaCollection();
            $analysis->addMedia($request->file('pdf'))->toMediaLibrary();
        }

        flashMessage('File saved successfully.');

        return redirect()->back();
    }

    public function removeFile($id) {
        $analysis = Analysis::findOrFail($id);

        $analysis->clearMediaCollection();

        flashMessage('File removed successfully.');

        return redirect()->back();
    }

    protected function getInput(Request $request)
    {
        $input = $request->only('name', 'code', 'patient_id', 'status');

        return $input;
    }
}