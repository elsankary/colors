<?php
namespace App\Http\Controllers\Backend;

use App\Models\Article;
use Illuminate\Http\Request;

class ArticlesController extends BackendController
{
    public function __construct()
    {
        $this->setActiveGroup('events_and_news');

        parent::__construct();
    }

    public function index(Request $request)
    {
        $this->setActivePage($request->get('type') == Article::TYPE_PUBLICATIONS ? 'publications' : 'news');

        $articles = Article::where('type', $request->get('type'))->get();

        return view('backend.articles.index', [
            'articles' => $articles,
            'type' => $request->get('type')
        ]);
    }

    public function store(Request $request)
    {
        $input = $this->getInput($request);

        $article = new Article($input);
        $article->save();

        flashMessage('Article has been added successfully.');

        return redirect()->route('backend.articles.index', [
            'type' => $request->get('type')
        ]);
    }

    public function update($id, Request $request)
    {
        $input = $this->getInput($request);

        $article = Article::findOrFail($id);
        $article->fill($input)->save();

        flashMessage('Article has been updated successfully.');

        return redirect()->route('backend.articles.index', [
            'type' => $request->get('type')
        ]);
    }

    public function destroy($id)
    {
        Article::destroy($id);

        flashMessage('Article has been deleted successfully.');

        return redirect()->back();
    }

    protected function getInput(Request $request)
    {
        $input = $request->only('title', 'body', 'published', 'type');

        return $input;
    }
}