<?php
namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;

class AuthController extends BackendController
{
    public function showLogin()
    {
        if (auth('backend')->check()) {
            return redirect()->route('backend.admins.index');
        }

        return view('backend.login');
    }

    public function doLogin(Request $request)
    {
        $creds = $request->only('email', 'password');
        $rememberMe = $request->has('remember_me');

        if (auth('backend')->attempt($creds, $rememberMe)) {
            return redirect()->route('backend.admins.index');
        }

        flashMessage('Email and password don\'t match.', 'danger');
        return redirect()->back();
    }

    public function doLogout()
    {
        auth('backend')->logout();
        return redirect()->route('backend.auth.show_login');
    }

}