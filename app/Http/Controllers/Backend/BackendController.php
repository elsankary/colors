<?php
namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;

class BackendController extends Controller
{
    public function __construct()
    {

    }

    public function home()
    {
        return redirect()->route('backend.auth.show_login');
    }

    public function setActiveGroup($slug)
    {
        session()->flash('backend_active_group', $slug);
    }

    public function setActivePage($slug)
    {
        session()->flash('backend_active_page', $slug);
    }
}