<?php
namespace App\Http\Controllers\Backend;

use App\Models\Banner;
use Illuminate\Http\Request;

class BannersController extends BackendController
{
    public function __construct()
    {
        $this->setActiveGroup('banners');

        parent::__construct();
    }

    public function index()
    {
        $banners = Banner::with('media')->orderBy('created_at', 'DESC')->get();

        $pages = ['about' => 'about', 'news' => 'News' , 'events' => 'events', 'publications' => 'publications', 'calculator' => 'calculator'];

        return view('backend.banners.index', compact('banners','pages'));
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'from'=> 'required|date|date_format:Y-m-d|after:yesterday',
            'to'=> 'required|date|date_format:Y-m-d|after:from',
            'image' => 'required|mimes:jpg,jpeg,gif,png',
        ]);

        $input = $this->getInput($request);
        //dd($input);
        $input['position'] = json_encode($input['position']);

        $banner = new Banner($input);
        $banner->save();

        if ($request->hasFile('image')) {
            $banner->addMedia($request->file('image'))->toMediaLibrary();
        }

        flashMessage('Banner has been added successfully.');

        return redirect()->route('backend.banners.index');
    }

    public function update($id, Request $request)
    {
        $this->validate($request, [
            'from'=> 'required|date|date_format:Y-m-d',
            'to'=> 'required|date|date_format:Y-m-d|after:from',
            'image' => 'required|mimes:jpg,jpeg,gif,png',
        ]);

        $input = $this->getInput($request);
        $input['position'] = json_encode($input['position']);

        $banner = Banner::findOrFail($id);
        $banner->fill($input)->save();

        if ($request->hasFile('image')) {
            $banner->clearMediaCollection();
            $banner->addMedia($request->file('image'))->toMediaLibrary();
        }

        flashMessage('Banner has been updated successfully.');

        return redirect()->route('backend.banners.index');
    }

    public function destroy($id)
    {
        Banner::destroy($id);

        flashMessage('Banner has been deleted successfully.');

        return redirect()->back();
    }

    protected function getInput(Request $request)
    {
        $input = $request->only('name', 'position', 'from', 'to');

        return $input;
    }

    public function changeState($id)
    {
        $banner = Banner::findOrFail($id);

        if($banner->isEnable()) {
            $banner->state = Banner::DISABLE;
        } else {
            $banner->state = Banner::ENABLE;
        }

        $banner->save();

        flashMessage('Banner has been '.$banner->state.' successfully.');

        return redirect()->back();
    }
}