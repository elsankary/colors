<?php
namespace App\Http\Controllers\Backend;

use App\Models\Doctor;
use Illuminate\Http\Request;

class DoctorsController extends BackendController
{
    public function __construct()
    {
        $this->setActiveGroup('accounts');
        $this->setActivePage('doctors');

        parent::__construct();
    }

    public function index()
    {
        $doctors = Doctor::get();
        return view('backend.doctors.index', compact('doctors'));
    }

    public function show($id)
    {
        $doctor = Doctor::findOrFail($id);

        $doctor->load('patients.analyses');

        return view('backend.doctors.show', compact('doctor'));
    }

    public function store(Request $request)
    {
        $input = $this->getInput($request);

        $doctor = new Doctor($input);
        $doctor->save();

        flashMessage('Doctor has been added successfully.');

        return redirect()->route('backend.doctors.index');
    }

    public function update($id, Request $request)
    {
        $input = $this->getInput($request);

        if (!$request->has('password')) {
            unset($input['password']);
        }

        $doctor = Doctor::findOrFail($id);
        $doctor->fill($input)->save();

        flashMessage('Doctor has been updated successfully.');

        return redirect()->route('backend.doctors.index');
    }

    public function destroy($id)
    {
        Doctor::destroy($id);

        flashMessage('Doctor has been deleted successfully.');

        return redirect()->route('backend.doctors.index');
    }

    protected function getInput(Request $request)
    {
        $input = $request->only(
            'email', 'name', 'code', 'national_id', 'phone_number',
            'clinic_name', 'clinic_phone_number',
            'affiliation', 'referred_by', 'speciality'
        );
        $input['password'] = bcrypt($request->get('password'));
        $input['medical'] = $request->has('medical');
        $input['research'] = $request->has('research');

        return $input;
    }
}