<?php
namespace App\Http\Controllers\Backend;

use App\Models\Event;
use Illuminate\Http\Request;

class EventsController extends BackendController
{
    public function __construct()
    {
        $this->setActiveGroup('events_and_news');
        $this->setActivePage('events');
    }

    public function index()
    {
        $events = Event::get();
        return view('backend.events.index', compact('events'));
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'from'=> 'required|date|date_format:Y-m-d|after:yesterday',
            'to'=> 'required|date|date_format:Y-m-d|after:from',
        ]);
        $input = $this->getInput($request);

        $event = new Event($input);
        $event->save();

        flashMessage('Event has been added successfully.');

        return redirect()->route('backend.events.index');
    }

    public function update($id, Request $request)
    {
        $this->validate($request, [
            'from'=> 'required|date|date_format:Y-m-d',
            'to'=> 'required|date|date_format:Y-m-d|after:from',
        ]);

        $input = $this->getInput($request);

        $event = Event::findOrFail($id);
        $event->fill($input)->save();

        flashMessage('Event has been updated successfully.');

        return redirect()->route('backend.events.index');
    }

    public function destroy($id)
    {
        Event::destroy($id);

        flashMessage('Event has been deleted successfully.');

        return redirect()->back();
    }

    protected function getInput(Request $request)
    {
        $input = $request->only('name', 'description', 'external_link', 'category', 'location', 'from', 'to');

        return $input;
    }
}