<?php
namespace App\Http\Controllers\Backend;

use Spatie\MediaLibrary\Media;

class MediaController extends BackendController
{
    public function destroy($id)
    {
        return Media::destroy($id);
    }
}