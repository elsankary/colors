<?php
namespace App\Http\Controllers\Backend;

use App\Models\Order;

class OrdersController extends BackendController
{
    public function __construct()
    {
        $this->setActiveGroup('orders');

        parent::__construct();
    }

    public function calculator()
    {
        $this->setActivePage('calculator');

        $orders = Order::with('orderable')->where('type', 'calculator')->latest()->get();

        return view('backend.orders.calculator', compact('orders'));
    }

    public function researches()
    {
        $this->setActivePage('research');

        $orders = Order::with('orderable')->where('type', 'research')->latest()->get();

        return view('backend.orders.researches', compact('orders'));
    }

    public function patients()
    {
        $this->setActivePage('patients');

        $orders = Order::with('orderable.patient.doctor', 'orderable.media')
            ->where('type', 'patients')
            ->latest()
            ->get();

        return view('backend.orders.patients', compact('orders'));
    }

    public function destroy($id)
    {
        Order::destroy($id);

        flashMessage('Order has been deleted successfully.');

        return redirect()->back();
    }
}