<?php
namespace App\Http\Controllers\Backend;

use App\Models\Page;
use App\Support\YoutubeVideo;
use Illuminate\Http\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class PagesController extends BackendController
{
    public function __construct()
    {
        $this->setActiveGroup('pages');

        parent::__construct();
    }

    public function edit($key)
    {
        if ($key == Page::KEY_HOME) {
            return $this->editHomePage();
        } else if ($key == Page::KEY_ABOUT) {
            return $this->editAboutPage();
        } else if ($key == Page::KEY_CONTACT) {
            return $this->editContactPage();
        }

        throw new NotFoundHttpException();
    }

    protected function editHomePage()
    {
        $this->setActivePage('home');

        $page = Page::findByKeyOrFail(Page::KEY_HOME);

        $video = new YoutubeVideo($page->content('video'));
        $body = $page->content('body');

        return view('backend.pages.edit_home', compact('video', 'body', 'page'));
    }

    protected function editAboutPage()
    {
        $this->setActivePage('about');

        $page = Page::findByKeyOrFail(Page::KEY_ABOUT);

        $videos = [];
        foreach ($page->content('videos') as $videoUrl) {
            $videos[] = new YoutubeVideo($videoUrl);
        }

        $images = $page->getMedia()->map(function ($item) {
            return [
                'id' => $item->id,
                'url' => $item->getUrl()
            ];
        });

        $body = $page->content('body');

        return view('backend.pages.edit_about', compact('videos', 'images', 'body', 'page'));
    }

    protected function editContactPage()
    {
        $this->setActivePage('contact-us');

        $page = Page::findByKeyOrFail(Page::KEY_CONTACT);

        return view('backend.pages.edit_contact', $page->content());
    }

    public function update($key, Request $request)
    {
        if ($key == Page::KEY_HOME) {
            return $this->updateHomePage($request);
        } else if ($key == Page::KEY_ABOUT) {
            return $this->updateAboutPage($request);
        } else if ($key == Page::KEY_CONTACT) {
            return $this->updateContactPage($request);
        }

        throw new NotFoundHttpException();
    }

    protected function updateHomePage(Request $request)
    {
        $page = Page::findByKeyOrFail(Page::KEY_HOME);

        $toBeUpdated = [
            'body' => $request->get('body'),
            'video' => $request->get('videos')[0]
        ];

        $page->content = json_encode($toBeUpdated);
        $page->save();

        flashMessage('Page has been updated successfully.');
        return redirect()->route('backend.pages.edit', Page::KEY_HOME);
    }

    protected function updateAboutPage(Request $request)
    {
        $page = Page::findByKeyOrFail(Page::KEY_ABOUT);

        $toBeUpdated = [
            'body' => $request->get('body'),
            'videos' => $request->get('videos')
        ];

        foreach ($request->file('images') as $image) {
            $page->addMedia($image)->toMediaLibrary();
        }

        $page->content = json_encode($toBeUpdated);
        $page->save();

        flashMessage('Page has been updated successfully.');
        return redirect()->route('backend.pages.edit', Page::KEY_ABOUT);
    }

    protected function updateContactPage(Request $request)
    {
        $page = Page::findByKeyOrFail(Page::KEY_CONTACT);

        $toBeUpdated = $request->only(
            'phone_number', 'address', 'email', 'facebook', 'linkedin', 'youtube',
            'latitude', 'longitude', 'description'
        );

        $page->content = json_encode($toBeUpdated);
        $page->save();

        flashMessage('Page has been updated successfully.');
        return redirect()->route('backend.pages.edit', Page::KEY_CONTACT);
    }
}