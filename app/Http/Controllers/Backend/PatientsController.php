<?php
namespace App\Http\Controllers\Backend;

use App\Models\Doctor;
use App\Models\Patient;
use Illuminate\Http\Request;

class PatientsController extends BackendController
{
    public function __construct()
    {
        $this->setActiveGroup('accounts');
        $this->setActivePage('patients');

        parent::__construct();
    }

    public function index()
    {
        $patients = Patient::with('analyses', 'doctor')->latest()->get();
        $doctors = Doctor::get();
        return view('backend.patients.index', compact('patients', 'doctors'));
    }

    public function show($id)
    {
        $patient = Patient::findOrFail($id);

        $patient->load(['analyses' => function ($query) {
            return $query->latest();
        }]);

        return view('backend.patients.show', compact('patient'));
    }

    public function store(Request $request)
    {
        $input = $this->getInput($request);

        $patient = new Patient($input);
        $patient->save();

        flashMessage('Patient has been added successfully.');

        return redirect()->back();
    }

    public function update($id, Request $request)
    {
        $input = $this->getInput($request);

        if (!$request->has('password')) {
            unset($input['password']);
        }

        $patient = Patient::findOrFail($id);
        $patient->fill($input)->save();

        flashMessage('Patient has been updated successfully.');

        return redirect()->back();
    }

    public function destroy($id)
    {
        Patient::destroy($id);

        flashMessage('Patient has been deleted successfully.');

        return redirect()->back();
    }

    protected function getInput(Request $request)
    {
        $input = $request->only('name', 'code', 'national_id', 'phone_number', 'age', 'doctor_id');

        if ($request->has('password')) {
            $input['password'] = bcrypt($request->get('password'));
        }

        return $input;
    }
}