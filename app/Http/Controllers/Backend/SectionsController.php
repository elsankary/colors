<?php
namespace App\Http\Controllers\Backend;

use App\Models\Section;
use Illuminate\Http\Request;

class SectionsController extends BackendController
{
    public function __construct()
    {
        $this->setActiveGroup('sections');

        parent::__construct();
    }

    public function index()
    {
        $sections = Section::with('ancestors')->get();

        $nodes = Section::where('type',Section::TYPE_MEDICAL)->get()->toTree();

        $tree = Section::listSections($nodes);

        return view('backend.sections.index', compact('sections', 'tree'));
    }

    public function store(Request $request)
    {
        $input = $this->getInput($request);

        if($input['parent_id']){
            $parent = Section::findOrFail($input['parent_id']);
            $parent->child_flag = 1;
            $parent->save();
        }

        $section = new Section($input);
        $section->save();

        flashMessage('Section has been added successfully.');

        return redirect()->route('backend.sections.index');
    }

    public function edit($id)
    {
        $section = Section::find($id);

        $nodes = Section::where('type',$section->type)->get()->toTree();

        $tree = Section::listSections($nodes);

        return view('backend.sections._edit',compact('section','tree'));
    }

    public function update($id, Request $request)
    {
        $input = $this->getInput($request);

        $section = Section::findOrFail($id);
        $section->fill($input)->save();

        flashMessage('Section has been updated successfully.');

        return redirect()->route('backend.sections.index');
    }

    public function destroy($id)
    {
        Section::destroy($id);

        flashMessage('Section has been deleted successfully.');

        return redirect()->back();
    }

    protected function getInput(Request $request)
    {
        $input = $request->only('type', 'name', 'description', 'price', 'parent_id');

        return $input;
    }

    public function getTree(Request $request)
    {
        if ($request->type == 'medical') {
            $nodes = Section::where('type',Section::TYPE_MEDICAL)->get()->toTree();
            $tree = Section::listSections($nodes);
        } elseif ($request->type == 'research') {
            $nodes = Section::where('type',Section::TYPE_RESEARCH)->get()->toTree();
            $tree = Section::listSections($nodes);
        }

        $data = view('backend.sections._tree',compact('tree'))->render();
        return $data;
    }
}