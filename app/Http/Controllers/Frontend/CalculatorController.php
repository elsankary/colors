<?php

namespace App\Http\Controllers\Frontend;


use App\Models\Order;
use App\Models\Section;
use Illuminate\Http\Request;

class CalculatorController extends FrontendController
{
    public function home()
    {
        $this->setActivePage('calculator');

        $tree = Section::where('type', Section::TYPE_MEDICAL)->get()->toTree();

        //$tree = Section::getSections($tree);

        return view('frontend.calculator.home', compact('tree'));
    }

    public function tree(Request $request)
    {
        $tree = Section::where('type', $request->get('type'))->get()->toTree();

        return view('frontend.calculator._tree', compact('tree'))->render();
    }

    public function request(Request $request)
    {
        $input = $request->only('name', 'phone_number', 'email', 'university', 'college', 'department', 'notes');

        $input['sections_details'] = '';
        $input['total'] = 0;
        foreach ($request->sections as $key => $value) {
            $section = Section::findOrFail($value);
            if ($section->child_flag != 1) {
                $input['sections_details'] = $input['sections_details'] . '( ' . $section->name . ' - $' . $section->price.' ),' ;
                $input['total'] = $input['total'] + $section->price;
            }
        }

        $order = new Order(['meta' => json_encode($input)]);
        $order->new = 1;
        $order->type = 'calculator';

        if (auth('doctor')->check()) {
            $order->orderable_id = auth('doctor')->user()->id;
            $order->orderable_type = 'App\Models\Doctor';
        }

        $order->save();

        flashMessage('Order saved successfully.');
        return redirect()->back();
    }
}