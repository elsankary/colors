<?php
namespace App\Http\Controllers\Frontend;

use App\Models\Analysis;
use App\Models\Doctor;
use App\Models\Order;
use App\Models\Patient;
use App\Models\Section;
use Illuminate\Http\Request;

class DoctorController extends FrontendController
{
    public function home()
    {
        $this->setActivePage('doctor');

        $tree = Section::where('type', Section::TYPE_MEDICAL)->get()->toTree();

        return view('frontend.doctor.home', compact('tree'));
    }

    public function register(Request $request)
    {
        $input = $request->only(
            'name', 'email', 'password', 'phone_number', 'national_id', 'clinic_name', 'clinic_phone_number', 'speciality'
        );
        $input['password'] = bcrypt($request->get('password'));
        $input['medical'] = 1;

        $doctor = new Doctor($input);
        $doctor->save();

        $doctor->code = 'DO' . $doctor->id;
        $doctor->save();

        auth('doctor')->login($doctor);

        return redirect('/');
    }

    public function login(Request $request)
    {
        $creds = $request->only('email', 'password');

        if (auth('doctor')->attempt($creds, true)) {
            return redirect()->to('/doctor/dashboard');
        }

        return redirect()->back();
    }

    public function logout()
    {
        auth('doctor')->logout();

        return redirect('/');
    }

    public function dashboard()
    {
        $patients = auth('doctor')->user()->patients;
        $patients = $patients->sortByDesc(function ($item) {
            return $item->created_at->timestamp;
        });
        $patients->load('analyses');

        return view('frontend.doctor.dashboard', compact('patients'));
    }

    public function addPatient(Request $request)
    {
        $input = $request->only('name', 'national_id', 'phone_number', 'age');
        $input['doctor_id'] = auth('doctor')->id();
        $input['code'] = 'PA' . time();

        $patient = new Patient($input);
        $patient->save();

        $analysis = $this->saveAnalysis($patient, $request->get('analysis'));

        $this->saveOrder($patient, $analysis);

        return redirect()->back();
    }

    public function addAnalysis(Request $request)
    {
        $patient = Patient::findOrFail($request->get('patient_id'));

        $analysis = $this->saveAnalysis($patient, $request->get('name'));

        $this->saveOrder($patient, $analysis);

        flashMessage('Analysis has been added successfully.');
        return redirect()->back();

    }

    public function saveAnalysis(Patient $patient, $name)
    {
        $existingAnalysis = Analysis::where('name', $name)->first();

        return $patient->analyses()->create([
            'code' => $existingAnalysis ? $existingAnalysis->code : '',
            'name' => $name
        ]);
    }

    protected function saveOrder(Patient $patient, Analysis $analysis)
    {
        $order = new Order([
            'type' => 'patients',
            'meta' => json_encode([
                'patient_name' => $patient->name,
                'patient_phone_number' => $patient->phone_number,
                'analysis_name' => $analysis->name,
                'doctor_name' => auth('doctor')->user()->name
            ]),
            'new' => 1,
            'orderable_id' => $analysis->id,
            'orderable_type' => 'App\Models\Analysis'
        ]);
        $order->save();
    }
}