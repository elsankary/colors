<?php
namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Models\Page;

class FrontendController extends Controller
{

    public function __construct()
    {
        $page = Page::findByKeyOrFail(Page::KEY_CONTACT);
        $shared = [
            'facebook' => $page->content('facebook'),
            'linkedin' => $page->content('linkedin'),
            'youtube' => $page->content('youtube'),
        ];
        view()->share('shared', $shared);
    }

    public function setActivePage($slug)
    {
        session()->flash('frontend_active_page', $slug);
    }

}