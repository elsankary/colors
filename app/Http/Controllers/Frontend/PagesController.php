<?php
namespace App\Http\Controllers\Frontend;

use App\Models\Article;
use App\Models\Event;
use App\Models\Page;
use App\Models\Section;
use App\Support\YoutubeVideo;
use Carbon\Carbon;
use Kalnoy\Nestedset\Collection;

class PagesController extends FrontendController
{

    public function home()
    {
        $this->setActivePage('home');

        $page = Page::findByKeyOrFail(Page::KEY_HOME);
        $video = new YoutubeVideo($page->content('video'));

        return view('frontend.home', compact('page', 'video'));
    }

    public function about()
    {
        $this->setActivePage('about');

        $page = Page::findByKeyOrFail(Page::KEY_ABOUT);

        $videos = new Collection();
        foreach ($page->content('videos') as $videoUrl) {
            $videos->push(new YoutubeVideo($videoUrl));
        }

        return view('frontend.about', compact('page', 'videos'));
    }

    public function contact()
    {
        $this->setActivePage('contact');

        $page = Page::findByKeyOrFail(Page::KEY_CONTACT);

        return view('frontend.contact', compact('page'));
    }

    public function news()
    {
        $this->setActivePage('news');

        $articles = Article::where('type', Article::TYPE_NEWS)->where('published', 1)->latest()->get();

        return view('frontend.news', compact('articles'));
    }

    public function publications()
    {
        $this->setActivePage('news');

        $articles = Article::where('type', Article::TYPE_PUBLICATIONS)->where('published', 1)->latest()->get();

        return view('frontend.publications', compact('articles'));
    }

    public function article($id)
    {
        $this->setActivePage('news');

        $article = Article::findOrFail($id);

        return view('frontend.article', compact('article'));
    }

    public function events()
    {
        $this->setActivePage('events');

        $events = Event::where('to', '>', Carbon::now())->get();

        return view('frontend.events', compact('events'));
    }
}