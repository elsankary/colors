<?php
namespace App\Http\Controllers\Frontend;

use App\Models\Analysis;
use Illuminate\Http\Request;

class PatientController extends FrontendController
{
    public function home()
    {
        $this->setActivePage('patient');

        return view('frontend.patient.home');
    }

    public function login(Request $request)
    {
        $creds = $request->only('phone_number', 'password');

        if (auth('patient')->attempt($creds, true)) {
            return redirect()->to('/patient/dashboard');
        }

        return redirect()->back();
    }

    public function logout()
    {
        auth('patient')->logout();

        return redirect('/');
    }

    public function dashboard()
    {
        $analyses = Analysis::with('patient', 'media')
            ->where('patient_id', auth('patient')->id())
            ->get();
        $analyses = $analyses->sortByDesc(function ($item) {
            return $item->created_at->timestamp;
        });

        return view('frontend.patient.dashboard', compact('analyses'));
    }
}