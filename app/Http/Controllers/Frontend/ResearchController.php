<?php
namespace App\Http\Controllers\Frontend;

use App\Models\Order;
use App\Models\Section;
use Illuminate\Http\Request;

class ResearchController extends FrontendController
{
    public function home()
    {
        $this->setActivePage('research');

        $tree = Section::where('type', Section::TYPE_RESEARCH)->get()->toTree();

        return view('frontend.research.home', compact('tree'));
    }

    public function request(Request $request)
    {
        $section = Section::findOrFail($request->get('section_id'));
        $input = $request->only('name', 'phone_number', 'email', 'university', 'college', 'department', 'notes');

        $input['sections_details'] = '';
        $input['total'] = 0;
        if ($section->child_flag == 1) {
            $child_sections = Section::where('parent_id',$section->id)->get();
            foreach ($child_sections as $child_section) {
                $input['sections_details'] = $input['sections_details'].'( ' . $child_section->name . ' - $' . $child_section->price.' ),' ;
                $input['total'] = $input['total'] + $child_section->price;
            }
        } else {
            $input['sections_details'] = '( ' . $section->name . ' - $' . $section->price.' ),' ;
            $input['total'] = $section->price;
        }

        $order = new Order(['meta' => json_encode($input)]);
        $order->new = 1;
        $order->type = 'research';

        if (auth('doctor')->check()) {
            $order->orderable_id = auth('doctor')->user()->id;
            $order->orderable_type = 'App\Models\Doctor';
        }

        $order->save();

        flashMessage('Order saved successfully.');
        return redirect()->back();
    }
}