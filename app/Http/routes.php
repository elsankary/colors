<?php

Route::group(['namespace' => 'Frontend'], function () {
    Route::get('/', ['as' => 'frontend.home', 'uses' => 'PagesController@home']);
    Route::get('/about', ['as' => 'frontend.about', 'uses' => 'PagesController@about']);
    Route::get('/contact', ['as' => 'frontend.contact', 'uses' => 'PagesController@contact']);
    Route::get('/news', ['as' => 'frontend.news', 'uses' => 'PagesController@news']);
    Route::get('/publications', ['as' => 'frontend.publications', 'uses' => 'PagesController@publications']);
    Route::get('/articles/{id}', ['as' => 'frontend.article', 'uses' => 'PagesController@article']);
    Route::get('/events', ['as' => 'frontend.contact', 'uses' => 'PagesController@events']);

    Route::get('/doctor', ['as' => 'frontend.doctor.home', 'uses' => 'DoctorController@home']);
    Route::post('/doctor/register', ['as' => 'frontend.doctor.register', 'uses' => 'DoctorController@register']);
    Route::post('/doctor/login', ['as' => 'frontend.doctor.login', 'uses' => 'DoctorController@login']);
    Route::get('/doctor/logout', ['as' => 'frontend.doctor.logout', 'uses' => 'DoctorController@logout']);

    Route::group(['middleware' => 'auth:doctor,/doctor'], function(){
        Route::get('/doctor/dashboard', ['as' => 'frontend.doctor.dashboard', 'uses' => 'DoctorController@dashboard']);
        Route::post('/doctor/add-patient', ['as' => 'frontend.doctor.add_patient', 'uses' => 'DoctorController@addPatient']);
        Route::post('/doctor/add-analysis', ['as' => 'frontend.doctor.add_analysis', 'uses' => 'DoctorController@addAnalysis']);
    });

    Route::get('/patient', ['as' => 'frontend.patient.home', 'uses' => 'PatientController@home']);
    Route::post('/patient/login', ['as' => 'frontend.patient.login', 'uses' => 'PatientController@login']);
    Route::get('/patient/logout', ['as' => 'frontend.patient.logout', 'uses' => 'PatientController@logout']);
    Route::get('/patient/dashboard', ['as' => 'frontend.patient.dashboard', 'uses' => 'PatientController@dashboard'])
        ->middleware('auth:patient,/patient');

    Route::get('/research', ['as' => 'frontend.research.home', 'uses' => 'ResearchController@home']);
    Route::post('/research/request', ['as' => 'frontend.research.request', 'uses' => 'ResearchController@request']);

    Route::get('/calculator', ['as' => 'frontend.calculator.home', 'uses' => 'CalculatorController@home']);
    Route::get('/calculator/tree', ['as' => 'frontend.calculator.tree', 'uses' => 'CalculatorController@tree']);
    Route::post('/calculator/request', ['as' => 'frontend.calculator.request', 'uses' => 'CalculatorController@request']);
});

Route::group(['prefix' => 'backend', 'namespace' => 'Backend'], function () {
    Route::get('/', ['as' => 'backend.home', 'uses' => 'BackendController@home']);

    Route::get('/login', ['as' => 'backend.auth.show_login', 'uses' => 'AuthController@showLogin']);
    Route::post('/login', ['as' => 'backend.auth.do_login', 'uses' => 'AuthController@doLogin']);
    Route::get('/logout', ['as' => 'backend.auth.do_logout', 'uses' => 'AuthController@doLogout']);

    Route::group(['middleware' => 'auth:backend,/backend/login'], function () {
        Route::get('admins', ['as' => 'backend.admins.index', 'uses' => 'AdminsController@index']);
        Route::post('admins', ['as' => 'backend.admins.store', 'uses' => 'AdminsController@store']);
        Route::patch('admins/{id}', ['as' => 'backend.admins.update', 'uses' => 'AdminsController@update']);
        Route::delete('admins/{id}', ['as' => 'backend.admins.destroy', 'uses' => 'AdminsController@destroy']);

        Route::get('doctors', ['as' => 'backend.doctors.index', 'uses' => 'DoctorsController@index']);
        Route::post('doctors', ['as' => 'backend.doctors.store', 'uses' => 'DoctorsController@store']);
        Route::get('doctors/{id}', ['as' => 'backend.doctors.show', 'uses' => 'DoctorsController@show']);
        Route::patch('doctors/{id}', ['as' => 'backend.doctors.update', 'uses' => 'DoctorsController@update']);
        Route::delete('doctors/{id}', ['as' => 'backend.doctors.destroy', 'uses' => 'DoctorsController@destroy']);

        Route::get('patients', ['as' => 'backend.patients.index', 'uses' => 'PatientsController@index']);
        Route::post('patients', ['as' => 'backend.patients.store', 'uses' => 'PatientsController@store']);
        Route::get('patients/{id}', ['as' => 'backend.patients.show', 'uses' => 'PatientsController@show']);
        Route::patch('patients/{id}', ['as' => 'backend.patients.update', 'uses' => 'PatientsController@update']);
        Route::delete('patients/{id}', ['as' => 'backend.patients.destroy', 'uses' => 'PatientsController@destroy']);

        Route::post('analyses', ['as' => 'backend.analyses.store', 'uses' => 'AnalysesController@store']);
        Route::post('analyses/add-file', ['as' => 'backend.analyses.add_file', 'uses' => 'AnalysesController@addFile']);
        Route::get('analyses/remove-file/{id}', ['as' => 'backend.analyses.remove_file', 'uses' => 'AnalysesController@removeFile']);
        Route::patch('analyses/{id}', ['as' => 'backend.analyses.update', 'uses' => 'AnalysesController@update']);
        Route::delete('analyses/{id}', ['as' => 'backend.analyses.destroy', 'uses' => 'AnalysesController@destroy']);

        Route::get('articles', ['as' => 'backend.articles.index', 'uses' => 'ArticlesController@index']);
        Route::post('articles', ['as' => 'backend.articles.store', 'uses' => 'ArticlesController@store']);
        Route::patch('articles/{id}', ['as' => 'backend.articles.update', 'uses' => 'ArticlesController@update']);
        Route::delete('articles/{id}', ['as' => 'backend.articles.destroy', 'uses' => 'ArticlesController@destroy']);

        Route::get('events', ['as' => 'backend.events.index', 'uses' => 'EventsController@index']);
        Route::post('events', ['as' => 'backend.events.store', 'uses' => 'EventsController@store']);
        Route::patch('events/{id}', ['as' => 'backend.events.update', 'uses' => 'EventsController@update']);
        Route::delete('events/{id}', ['as' => 'backend.events.destroy', 'uses' => 'EventsController@destroy']);

        Route::get('sections', ['as' => 'backend.sections.index', 'uses' => 'SectionsController@index']);
        Route::post('sections', ['as' => 'backend.sections.store', 'uses' => 'SectionsController@store']);
        Route::get('sections/edit/{id}', ['as' => 'backend.sections.edit', 'uses' => 'SectionsController@edit']);
        Route::post('sections/{id}', ['as' => 'backend.sections.update', 'uses' => 'SectionsController@update']);
        Route::delete('sections/{id}', ['as' => 'backend.sections.destroy', 'uses' => 'SectionsController@destroy']);
        Route::get('sections/getTree', ['as' => 'backend.sections.getTree' , 'uses' => 'SectionsController@getTree']);

        Route::get('banners', ['as' => 'backend.banners.index', 'uses' => 'BannersController@index']);
        Route::post('banners', ['as' => 'backend.banners.store', 'uses' => 'BannersController@store']);
        Route::patch('banners/{id}', ['as' => 'backend.banners.update', 'uses' => 'BannersController@update']);
        Route::delete('banners/{id}', ['as' => 'backend.banners.destroy', 'uses' => 'BannersController@destroy']);
        Route::get('banners/changeState/{id}', ['as' => 'backend.banners.state', 'uses' => 'BannersController@changeState']);

        Route::get('pages/{key}/edit', ['as' => 'backend.pages.edit', 'uses' => 'PagesController@edit']);
        Route::patch('pages/{key}', ['as' => 'backend.pages.update', 'uses' => 'PagesController@update']);

        Route::get('orders/calculator', ['as' => 'backend.orders.calculator', 'uses' => 'OrdersController@calculator']);
        Route::get('orders/patients', ['as' => 'backend.orders.patients', 'uses' => 'OrdersController@patients']);
        Route::get('orders/researches', ['as' => 'backend.orders.researches', 'uses' => 'OrdersController@researches']);
        Route::delete('orders/{id}', ['as' => 'backend.orders.destroy', 'uses' => 'OrdersController@destroy']);

        Route::delete('media/{id}', ['as' => 'backend.media.destroy', 'uses' => 'MediaController@destroy']);
    });
});