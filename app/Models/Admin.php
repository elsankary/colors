<?php
namespace App\Models;

use Illuminate\Foundation\Auth\User as Authenticatable;

class Admin extends Authenticatable
{
    const PERMISSION_FULL = 'full';
    const PERMISSION_FULL_WITHOUT_CONTENT = 'full_without_content';

    protected $fillable = [
        'code', 'name', 'email', 'password', 'phone_number', 'permission'
    ];
    protected $hidden = [
        'password', 'remember_token',
    ];
}
