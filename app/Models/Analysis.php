<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Spatie\MediaLibrary\HasMedia\Interfaces\HasMedia;

class Analysis extends Model implements HasMedia
{
    use HasMediaTrait;

    protected $fillable = [
        'name', 'code', 'status', 'patient_id'
    ];


    public function patient()
    {
        return $this->belongsTo(Patient::class);
    }
}
