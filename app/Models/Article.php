<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

class Article extends Model
{
    const TYPE_NEWS = 'news';
    const TYPE_PUBLICATIONS = 'publications';

    protected $fillable = [
        'title', 'body', 'type', 'published'
    ];


    public function scopeNews(Builder $builder)
    {
        return $builder->where('type', static::TYPE_NEWS);
    }

    public function scopePublications(Builder $builder)
    {
        return $builder->where('type', static::TYPE_PUBLICATION);
    }
}
