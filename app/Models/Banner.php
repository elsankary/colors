<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Laracasts\Presenter\PresentableTrait;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Spatie\MediaLibrary\HasMedia\Interfaces\HasMedia;

class Banner extends Model implements HasMedia
{
    use HasMediaTrait, PresentableTrait;

    protected $presenter = '\App\Models\Presenters\BannerPresenter';
    protected $fillable = [
        'name', 'position', 'from', 'to','state'
    ];
    protected $dates = ['from', 'to'];

    const ENABLE = 1;

    const DISABLE = 0;

    public function getState()
    {
        if ($this->state == static::ENABLE) {
            return 'disable';
        } else {
            return 'enable';
        }
    }

    public function isEnable()
    {
        return $this->state == static::ENABLE;
    }

    public function registerMediaConversions()
    {
        $this->addMediaConversion('thumb')
            ->width(368)
            ->height(232)
            ->sharpen(10)
            ->optimize();
    }
}
