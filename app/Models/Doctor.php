<?php
namespace App\Models;

use Illuminate\Foundation\Auth\User as Authenticatable;

class Doctor extends Authenticatable
{
    const PERMISSION_MEDICAL = 'medical';
    const PERMISSION_RESEARCH = 'research';

    protected $fillable = [
        'code', 'name', 'email', 'password', 'phone_number', 'national_id',
        'clinic_name', 'clinic_phone_number', 'affiliation', 'referred_by', 'speciality',
        'medical', 'research'
    ];
    protected $hidden = [
        'password', 'remember_token',
    ];


    public function patients()
    {
        return $this->hasMany(Patient::class);
    }
}
