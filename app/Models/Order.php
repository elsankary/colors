<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $fillable = [
        'meta', 'new', 'orderable_id', 'orderable_type', 'type'
    ];
    protected $metaFields;

    public function orderable()
    {
        return $this->morphTo();
    }

    public function meta($field, $default = '')
    {
        if (!$this->metaFields) {
            $this->metaFields = json_decode($this->attributes['meta'], true);
        }

        if (is_array($this->metaFields) && array_key_exists($field, $this->metaFields)) {
            return $this->metaFields[$field];
        }

        return $default;
    }
}
