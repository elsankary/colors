<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Spatie\MediaLibrary\HasMedia\Interfaces\HasMedia;

class Page extends Model implements HasMedia
{
    use HasMediaTrait;

    const KEY_HOME = 'home';
    const KEY_ABOUT = 'about';
    const KEY_CONTACT = 'contact';

    protected $fillable = [
        'key', 'title', 'content'
    ];


    public function content($key = null, $default = '')
    {
//        $decoded = json_decode($this->attributes['content'], true);
//
//        if (is_null($key)) return $decoded;
//
//        foreach ($decoded as $k => $v) {
//            if ($k == $key) return $v;
//        }

        return $default;
    }

    public static function findByKeyOrFail($key)
    {
        return static::where('key', $key)->first();
    }
}
