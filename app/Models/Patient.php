<?php
namespace App\Models;

use Illuminate\Foundation\Auth\User as Authenticatable;

class Patient extends Authenticatable
{
    protected $fillable = [
        'code', 'name', 'phone_number', 'national_id',
        'age', 'password', 'doctor_id'
    ];
    protected $hidden = [
        'password', 'remember_token',
    ];


    public function analyses()
    {
        return $this->hasMany(Analysis::class);
    }

    public function doctor()
    {
        return $this->belongsTo(Doctor::class);
    }

    public function orders()
    {
        return $this->morphMany('App\Models\Order', 'orderable');
    }
}
