<?php
namespace App\Models\Presenters;

use Laracasts\Presenter\Presenter;

class BannerPresenter extends Presenter
{
    public function from()
    {
        if ($this->entity->from) {
            return $this->entity->from->format('Y-m-d');
        }
    }

    public function to()
    {
        if ($this->entity->to) {
            return $this->entity->to->format('Y-m-d');
        }
    }
}