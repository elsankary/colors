<?php
namespace App\Models\Presenters;

use Laracasts\Presenter\Presenter;

class SectionPresenter extends Presenter
{
    public function parentsList()
    {
        return implode(' | ', $this->entity->ancestors->pluck('name')->toArray());
    }

    public function type()
    {
        return ucfirst($this->entity->type);
    }

    public function priceFormatted()
    {
        return $this->entity->price . ' EGP';
    }
}