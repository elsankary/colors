<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Kalnoy\Nestedset\NodeTrait;
use Laracasts\Presenter\PresentableTrait;

class Section extends Model
{
    use NodeTrait, PresentableTrait;

    const TYPE_MEDICAL = 'medical';
    const TYPE_RESEARCH = 'research';

    protected $fillable = [
        'type', 'name', 'description', 'price', 'parent_id'
    ];
    protected $presenter = 'App\Models\Presenters\SectionPresenter';


    public static function listSections($nodes)
    {
        $tree = [];

        $traverse = function ($sections, $prefix = '') use (&$traverse, &$tree) {
            foreach ($sections as $section) {
                $tree[$section->id] = $prefix . ' ' . $section->name;
                $traverse($section->children, $prefix . '-');
            }
        };

        $traverse($nodes);

        return $tree;
    }

    public static function getSections($nodes)
    {
        $tree = [];
        $key = 0;
        $traverse = function ($sections) use (&$traverse, &$tree,&$key) {
            foreach ($sections as $section) {
                $tree[$key] = $section;
                $key++;
                $traverse($section->children);
            }
        };

        $traverse($nodes);

        return $tree;
    }
}
