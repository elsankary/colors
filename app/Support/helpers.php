<?php

function flashMessage($content, $type = 'success')
{
    session()->flash('flash_message_type', $type);
    session()->flash('flash_message_content', $content);
}