<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDoctorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('doctors', function (Blueprint $table) {
            $table->increments('id');

            $table->string('code');
            $table->string('name');
            $table->string('email');
            $table->string('password');
            $table->string('national_id');
            $table->string('phone_number');
            $table->string('clinic_name');
            $table->string('clinic_phone_number');
            $table->string('affiliation');
            $table->string('referred_by');
            $table->string('speciality');
            $table->boolean('medical');
            $table->boolean('research');
            $table->rememberToken();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('doctors');
    }
}
