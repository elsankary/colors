<?php

use Illuminate\Database\Seeder;

class AdminsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Models\Admin::create([
            'code' => 'AD123456',
            'name' => 'Colors Admin',
            'email' => 'admin@colors-labs.com',
            'password' => bcrypt('123456'),
            'phone_number' => '01000000000',
            'permission' => \App\Models\Admin::PERMISSION_FULL
        ]);
    }
}
