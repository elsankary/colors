<?php

use App\Models\Page;
use Illuminate\Database\Seeder;

class CreateAboutPage extends Seeder
{
    public function run()
    {
        Page::create([
            'key' => Page::KEY_ABOUT,
            'title' => 'About Page',
            'content' => json_encode([
                'videos' => [
                    'https://www.youtube.com/watch?v=NpEaa2P7qZI'
                ],
                'body' => 'Sample Body'
            ])
        ]);
    }
}
