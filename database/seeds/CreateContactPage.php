<?php

use App\Models\Page;
use Illuminate\Database\Seeder;

class CreateContactPage extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Page::create([
            'key' => Page::KEY_CONTACT,
            'title' => 'Contact Page',
            'content' => json_encode([
                'phone_number' => '',
                'address' => '',
                'email' => '',
                'facebook' => '',
                'linkedin' => '',
                'youtube' => '',
                'latitude' => '',
                'longitude' => '',
                'description' => ''
            ])
        ]);
    }
}
