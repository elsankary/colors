<?php

use App\Models\Page;
use Illuminate\Database\Seeder;

class CreateHomePage extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Page::create([
            'key' => Page::KEY_HOME,
            'title' => 'Home Page',
            'content' => json_encode([
                'video' => 'https://www.youtube.com/watch?v=NpEaa2P7qZI',
                'body' => 'Sample Body'
            ])
        ]);
    }
}
