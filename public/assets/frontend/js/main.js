$(document).ready(function () {


    $('.category').click(function () {
        $(this).css({
            'border-color': '#fff',
            'color': '#fff',
            'background': 'linear-gradient(to right,  transparent 80%, #fff 20%)'
        }).siblings().css({
            'border-color': '#0087ff',
            'color': '#0087ff',
            'background': 'linear-gradient(to right,  transparent 80%, #0087ff 20%)'
        });
        /*$(".subblock").toggle();*/
    });


    $('ul.nav li.dropdown').hover(function () {
        $(this).find('.dropdown-menu').stop(true, true).delay(200).fadeIn(500);
    }, function () {
        $(this).find('.dropdown-menu').stop(true, true).delay(200).fadeOut(500);
    });

    $('.i-checks').iCheck({
        checkboxClass: 'icheckbox_square-green',
        radioClass: 'iradio_square-green',
    });
    // $(".fancybox-thumb").fancybox({
    //     prevEffect: 'none',
    //     nextEffect: 'none',
    //     helpers: {
    //         title: {
    //             type: 'outside'
    //         },
    //         thumbs: {
    //             width: 50,
    //             height: 50
    //         }
    //     }
    // });

    /* -------------------------------------------------------------------------*
 * WOW ANIMATION
 * -------------------------------------------------------------------------*/
    var wow = new WOW({
        boxClass: 'wow', // animated element css class (default is wow)
        animateClass: 'animated', // animation css class (default is animated)
        offset: 120, // distance to the element when triggering the animation (default is 0)
        mobile: true, // trigger animations on mobile devices (default is true)
        live: true, // act on asynchronously loaded content (default is true)
        callback: function (box) {
            // the callback is fired every time an animation is started
            // the argument that is passed in is the DOM node being animated
        }
    });
    $(window).load(function () {
        wow.init();
    });


    $('.carousel').carousel({
        interval: 6000
    });


});   // end jquery

