if (BLACKBURN == undefined || BLACKBURN == null || typeof(BLACKBURN) != "object") {
    var BLACKBURN = {};
}

$(document).ready(function () {
    $.ajaxSetup({
        headers: {"X-CSRF-TOKEN": $('meta[name=_token]').attr('content')}
    });

    $('.btn-create').on('click', function (e) {
        e.preventDefault();
        BLACKBURN.loadViewUsingAjax($(this).attr('href'));
    });

    $('.btn-edit').on('click', function (e) {
        e.preventDefault();
        BLACKBURN.loadViewUsingAjax($(this).data('url'));
    });

    $('.btn-delete').on('click', function (e) {
        e.preventDefault();
        $.form($(this).data('url'), {
            _method: 'DELETE',
            _token: $('meta[name=_token]').attr('content')
        }).submit();
    });

    $('.logout').on('click', function (e) {
        e.preventDefault();
        $.form($(this).attr('href'), {
            _token: $('meta[name=_token]').attr('content')
        }).submit();
    });

    $.validator.setDefaults({
        highlight: function (element) {
            $(element).closest('.form-group').addClass('has-error');
        },
        unhighlight: function (element) {
            $(element).closest('.form-group').removeClass('has-error');
        },
        errorPlacement: function () {
        }
    });
});

BLACKBURN.loadViewUsingAjax = function (url) {
    $.ajax({
        url: url,
        method: 'get',
        beforeSend: function () {
            BLACKBURN.loading();
        },
        success: function (response) {
            BLACKBURN.loaded();

            var m = $('#modal');
            m.find('.modal-body').html(response);
            m.modal('show');
        }
    });
};

BLACKBURN.loading = function () {
    $('#loading').addClass('start');
};

BLACKBURN.loaded = function () {
    $('#loading').removeClass('start');
};

$('.btn-print').on('click', function (e) {
    e.preventDefault();

    window.open($(this).data('url'), "Print Window", "scrollbars=yes,menubar=0,location=0,height=700,width=750");
});