var googleMapDiv = $('#googleMap');

var googleMapOptions = googleMapDiv.data('options');

// Map Markers
var mapMarkers = [{
    latitude: googleMapOptions.lat,
    longitude: googleMapOptions.lng,
    html: "<strong>" + googleMapOptions.company + "</strong>",
    icon: {
        image: googleMapOptions.icon,
        iconsize: [26, 46],
        iconanchor: [12, 46]
    },
    popup: true
}];

// Map Initial Location
var initLatitude = googleMapOptions.lat;
var initLongitude = googleMapOptions.lng;

// Map Extended Settings
var mapSettings = {
    controls: {
        draggable: true,
        panControl: true,
        zoomControl: true,
        mapTypeControl: true,
        scaleControl: true,
        streetViewControl: true,
        overviewMapControl: true
    },
    scrollwheel: false,
    markers: mapMarkers,
    latitude: initLatitude,
    longitude: initLongitude,
    zoom: 16
};

var map = googleMapDiv.gMap(mapSettings);

// Map Center At
var mapCenterAt = function(options, e) {
    e.preventDefault();
    googleMapDiv.gMap("centerAt", options);
};