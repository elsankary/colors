<div id="{{ $id or 'modalCreate' }}" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h3 class="title">{{ $title or 'Add New Admin' }}</h3>
            </div>
            <div class="modal-body">
                <form action="{{ $url or route('backend.admins.store') }}" method="post">
                    {{ csrf_field() }}
                    @if(isset($method))
                        <input type="hidden" name="_method" value="{{ $method }}">
                    @endif
                    <div class="row">
                        <div class="col-sm-5 ">
                            <div class="form-group">
                                <input type="text" name="name" value="{{ old('name', $admin->name) }}"
                                       class="form-control" placeholder="Name"
                                >
                            </div>
                            <div class="form-group">
                                <input type="email" name="email" value="{{ old('email', $admin->email) }}"
                                       class="form-control" placeholder="Email"
                                >
                            </div>
                            <div class="form-group">
                                <input type="password" name="password" class="form-control" placeholder="Password">
                            </div>
                        </div>
                        <div class="col-sm-5 col-sm-offset-2 ">
                            <div class="form-group" style="font-size: 15px;line-height: 29px;">
                                <label class="main-color">ID</label>
                                <label>{{ $admin->code or 'AD' . time() }}</label>
                                <input type="hidden" name="code" value="{{ $admin->code or 'AD' . time() }}">
                            </div>
                            <div class="clearfix"></div>
                            <div class="form-group">
                                <input type="text" value="{{ old('phone_number', $admin->phone_number) }}"
                                       class="form-control" name="phone_number" placeholder="Phone Number"
                                >
                            </div>
                            <div class="form-group">
                                <input type="password" name="password_confirmation" class="form-control"
                                       placeholder="Password Confirmation"
                                >
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-4">
                                        <label class="secound-color" style="padding-left: 8px">Permission</label>
                                    </div>
                                    <div class="col-md-1">
                                        <label class="radio-inline">
                                            <input type="radio" name="permission" value="{{ App\Models\Admin::PERMISSION_FULL }}"
                                                    {{ old('permission', $admin->permission) == App\Models\Admin::PERMISSION_FULL
                                                        || empty($admin->permission)
                                                            ? 'checked'
                                                            : '' }}
                                            > Full
                                        </label>
                                    </div>
                                    <div class="col-md-5 col-sm-offset-2">
                                        <label class="radio-inline">
                                            <input type="radio" name="permission" value="{{ App\Models\Admin::PERMISSION_FULL_WITHOUT_CONTENT }}"
                                                    {{ old('permission', $admin->permission) == App\Models\Admin::PERMISSION_FULL_WITHOUT_CONTENT
                                                        ? 'checked'
                                                        : '' }}
                                            > Full Without Content
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <button class="btn btn-sm btn-warning btn-block m-t-n-xs" type="submit">
                                <strong>Submit</strong>
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>