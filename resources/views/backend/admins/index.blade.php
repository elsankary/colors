@extends('backend.layouts.default')

@section('content')
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <button class="btn btn-info btn-rounded" data-toggle="modal" data-target="#modalCreate">
                        Add New Admin <i class="fa fa-plus-circle" aria-hidden="true" style="padding-left: 22px"></i>
                    </button>
                    <br>
                    <h3 class="title m-t-sm">Admins</h3>
                </div>
                <div class="ibox-content">
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered table-hover dataTables-example" >
                            <thead>
                            <tr class="frist">
                                <th>ID</th>
                                <th>Name</th>
                                <th>Email</th>
                                <th>Mobile</th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($admins as $admin)
                                <tr class="gradeX">
                                    <td>{{ $admin->code }}</td>
                                    <td>{{ $admin->name }}</td>
                                    <td>{{ $admin->email }}</td>
                                    <td>{{ $admin->phone_number }}</td>
                                    <td>
                                        <button class="btn btn-danger btn-xs" data-toggle="modal" data-target="#modalDelete{{ $admin->id }}"
                                        >
                                            <span class="glyphicon glyphicon-trash"></span>
                                        </button>&nbsp;&nbsp;
                                        <button class="btn btn-primary btn-xs" data-toggle="modal" data-target="#modalEdit{{ $admin->id }}">
                                            <span class="glyphicon glyphicon-edit"></span>
                                        </button>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @include('backend.admins._modal', [
        'admin' => new App\Models\Admin()
    ])

    @foreach($admins as $admin)
        @include('backend.admins._modal', [
            'url' => route('backend.admins.update', $admin->id),
            'method' => 'PATCH',
            'id' => 'modalEdit' . $admin->id
        ])
    @endforeach

    @foreach($admins as $admin)
        @include('backend.common._confirm_delete_modal', [
            'id' => $admin->id,
            'url' => route('backend.admins.destroy', $admin->id)
        ])
    @endforeach
@endsection
