<div id="{{ $id or 'modalCreate' }}" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h3 class="title">{{ $title or 'Add New Article' }}</h3>
            </div>
            <div class="modal-body">
                <form action="{{ $url or route('backend.articles.store') }}" method="post">
                    {{ csrf_field() }}
                    @if(isset($method))
                        <input type="hidden" name="_method" value="{{ $method }}">
                    @endif

                    <input type="hidden" name="type" value="{{ $type }}">

                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label class="col-sm-3 control-label main-color" style="margin-top: 8px;">
                                    {{ $article->created_at ? $article->created_at->format('Y-m-d') : date('Y-m-d') }}
                                </label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control" name="title" placeholder="Title"
                                           value="{{ $article->title or '' }}"
                                    >
                                </div>
                            </div>

                            <div class="form-group m-t-50">
                                <div class="col-lg-12">
                                    <div class="ibox float-e-margins">
                                        <div class="ibox-content no-padding m-t">
                                            <textarea class="summernote" name="body" placeholder="Body">{{ $article->body or '' }}</textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row  m-t-50">
                                    <div class="col-md-2 m-t">
                                        <label class="secound-color" style="padding-left: 20px">
                                            Status
                                        </label>
                                    </div>
                                    <div class="col-md-3 m-t">
                                        <label class="radio-inline font-bold">
                                            <input type="radio" name="published" value="1"
                                                    {{ $article->published ? 'checked' : '' }}
                                            > Published
                                        </label>
                                    </div>
                                    <div class="col-md-3 m-t">
                                        <label class="radio-inline font-bold">
                                            <input type="radio" name="published" value="0"
                                                    {{ $article->published ? '' : 'checked' }}
                                            > Unpublished
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <button class="btn btn-sm btn-warning btn-block m-t-n-xs" type="submit">
                                <strong>Submit</strong>
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>