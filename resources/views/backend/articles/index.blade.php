@extends('backend.layouts.default')

@section('content')
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h3 class="title m-t-sm">
                        @if(request('type') == App\Models\Article::TYPE_NEWS)
                            News
                        @else
                            Publications
                        @endif
                    </h3>
                    <button class="btn btn-info btn-rounded" data-toggle="modal" data-target="#modalCreate">
                        Add New Article <i class="fa fa-plus-circle" aria-hidden="true" style="padding-left: 22px"></i>
                    </button>
                </div>
                <div class="ibox-content">
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered table-hover dataTables-example" >
                            <thead>
                            <tr class="frist">
                                <th>Date</th>
                                <th>Title</th>
                                <th>Published</th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($articles as $article)
                                <tr class="gradeX">
                                    <td>{{ $article->created_at->format('Y-m-d') }}</td>
                                    <td>{{ $article->title }}</td>
                                    <td>
                                        <i class="fa {{ $article->published ? 'fa-check' : 'fa-times' }}" aria-hidden="true"></i>
                                    </td>
                                    <td>
                                        <button class="btn btn-danger btn-xs" data-toggle="modal" data-target="#modalDelete{{ $article->id }}"
                                        >
                                            <span class="glyphicon glyphicon-trash"></span>
                                        </button>&nbsp;&nbsp;
                                        <button class="btn btn-primary btn-xs" data-toggle="modal" data-target="#modalEdit{{ $article->id }}">
                                            <span class="glyphicon glyphicon-edit"></span>
                                        </button>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @include('backend.articles._modal', [
        'article' => new App\Models\Article()
    ])

    @foreach($articles as $article)
        @include('backend.articles._modal', [
            'url' => route('backend.articles.update', $article->id),
            'method' => 'PATCH',
            'id' => 'modalEdit' . $article->id
        ])
    @endforeach

    @foreach($articles as $article)
        @include('backend.common._confirm_delete_modal', [
            'id' => $article->id,
            'url' => route('backend.articles.destroy', $article->id)
        ])
    @endforeach
@endsection
