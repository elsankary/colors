<div id="{{ $id or 'modalCreate' }}" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content modal-lg">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h3 class="title">{{ $title or 'Add New Banner' }}</h3>
            </div>
            <div class="modal-body">
                <form action="{{ $url or route('backend.banners.store') }}" method="post" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    @if(isset($method))
                        <input type="hidden" name="_method" value="{{ $method }}">
                    @endif
                    <div class="row">
                        <div class="col-sm-5 ">
                            <div class="form-group">
                                <input type="text" name="name" placeholder="Name" class="form-control"
                                       value="{{ old('name', $banner->name) }}"
                                >
                            </div>
                            <div class="form-group">

                                {!!  Form::select('position[]', $pages , old('positon', json_decode($banner->position)),
                                array('required','multiple' => true , 'id' => 'position' , 'class' => 'form-control position')) !!}

                            </div>
                        </div>
                        <div class="col-sm-7 ">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="input-group date">
                                        <span class="input-group-addon">
                                            <i class="fa fa-calendar"></i>
                                        </span>
                                            <input type="text" name="from" class="form-control" placeholder="From"
                                                   value="{{ old('from', $banner->present()->from) }}"
                                            >
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class=" input-group date">
                                        <span class="input-group-addon">
                                            <i class="fa fa-calendar"></i>
                                        </span>
                                            <input type="text" name="to" class="form-control" placeholder="To"
                                                   value="{{ old('to', $banner->present()->to) }}"
                                            >
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="ibox float-e-margins">
                                <div  class="fileinput fileinput-new input-group" data-provides="fileinput">
                                    <div class="form-control" data-trigger="fileinput">
                                        <i class="glyphicon glyphicon-file fileinput-exists"></i>
                                        <span class="fileinput-filename">
                                            {{ !$banner->media->isEmpty() ? $banner->media()->first()->file_name : '' }}
                                        </span>
                                    </div>
                                    <span class="input-group-addon btn btn-default btn-file"style="background-color: #5fc8f9; color: #136c96;">Browse</span>
                                    <input type="hidden">
                                    <input type="file" name="image">
                                </div>
                            </div>
                        </div>


                        <div class="col-md-12">
                            <button class="btn btn-sm btn-warning btn-block m-t-n-xs" type="submit">
                                <strong>Submit</strong>
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>