@extends('backend.layouts.default')

@section('content')
    <div class="ibox float-e-margins">
        <div class="ibox-title">
            <h3 class="title m-t-sm">
                Banners
                <button class="btn btn-info btn-rounded" data-toggle="modal" data-target="#modalCreate">
                    Add New Banners <i class="fa fa-plus-circle" aria-hidden="true" style="padding-left: 22px"></i>
                </button>
            </h3>
        </div>
        <div class="ibox-content">
            <div class="table-responsive">
                <table class="table table-striped table-bordered table-hover dataTables-example" >
                    <thead>
                    <tr class="frist">
                        <th class="hidden">id</th>
                        <th>Name</th>
                        <th>From</th>
                        <th>To</th>
                        <th>Position</th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($banners as $banner)
                        <tr class="gradeX">
                            <td class="hidden"> {{ $banner->id }}</td>
                            <td>{{ $banner->name }}</td>
                            <td>{{ $banner->from->format('Y-m-d') }}</td>
                            <td>{{ $banner->to->format('Y-m-d') }}</td>
                            <td>
                                @foreach(json_decode($banner->position) as $key => $value)
                                    {{ ucfirst($value)  }},
                                @endforeach
                            </td>
                            <td>
                                <a class="btn btn-default btn-xs" href="{{ route('backend.banners.state',$banner->id) }}">
                                    {{ $banner->getState() }}
                                </a>
                                <button class="btn btn-danger btn-xs" data-toggle="modal" data-target="#modalDelete{{ $banner->id }}"
                                >
                                    <span class="glyphicon glyphicon-trash"></span>
                                </button>&nbsp;&nbsp;
                                <button class="btn btn-primary btn-xs" data-toggle="modal" data-target="#modalEdit{{ $banner->id }}">
                                    <span class="glyphicon glyphicon-edit"></span>
                                </button>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>

    @include('backend.banners._modal', [
        'banner' => new App\Models\Banner
    ])

    @foreach($banners as $banner)
        @include('backend.banners._modal', [
            'url' => route('backend.banners.update', $banner->id),
            'method' => 'PATCH',
            'id' => 'modalEdit' . $banner->id
        ])
    @endforeach

    @foreach($banners as $banner)
        @include('backend.common._confirm_delete_modal', [
            'id' => $banner->id,
            'url' => route('backend.banners.destroy', $banner->id)
        ])
    @endforeach
@endsection

@section('js')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-3-typeahead/4.0.2/bootstrap3-typeahead.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-multiselect/0.9.13/js/bootstrap-multiselect.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-multiselect/0.9.13/css/bootstrap-multiselect.css" />
    <script>
        $(document).ready(function() {
            $('.position').multiselect({
                nonSelectedText: 'Select pages',
                enableFiltering: true,
                enableCaseInsensitiveFiltering: true,
                buttonWidth: '300px'
            });
        });
    </script>
@append