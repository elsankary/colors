<div class="modal fade" id="modalDelete{{ $id }}" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title" style="padding-left: 14px;">Confirm Delete</h4>
            </div>
            <div class="modal-body m-t">
                <p>Do you want to proceed?</p>
            </div>
            <div class="modal-footer">
                <form action="{{ $url }}" method="post">
                    {{ csrf_field() }}
                    <input type="hidden" name="_method" value="DELETE">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                    <button type="submit" class="btn btn-danger btn-ok">Delete</button>
                </form>
            </div>
        </div>
    </div>
</div>