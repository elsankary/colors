@section('js')
    @if(session()->has('flash_message_content'))
        <script>
            toastr.info('{{ session()->get("flash_message_content") }}');
        </script>
    @endif
@append