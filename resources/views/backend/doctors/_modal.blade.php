<div id="{{ $id or 'modalCreate' }}" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h3 class="title">{{ $title or 'Add New Doctor' }}</h3>
            </div>
            <div class="modal-body">
                <form action="{{ $url or route('backend.doctors.store') }}" method="post">
                    {{ csrf_field() }}
                    @if(isset($method))
                        <input type="hidden" name="_method" value="{{ $method }}">
                    @endif
                    <div class="row">
                        <div class="col-sm-5">
                            <div class="form-group">
                                <input type="text" placeholder="Name" class="form-control" name="name"
                                       value="{{ old('name', $doctor->name) }}"
                                >
                            </div>
                            <div class="form-group">
                                <input type="text" placeholder="National Id" class="form-control" name="national_id"
                                       value="{{ old('national_id', $doctor->national_id) }}"
                                >
                            </div>
                            <div class="form-group">
                                <input type="email" placeholder="Email" class="form-control" name="email"
                                       value="{{ old('email', $doctor->email) }}"
                                >
                            </div>
                            <div class="form-group">
                                <input type="text" placeholder="Clinic Name" class="form-control" name="clinic_name"
                                       value="{{ old('clinic_name', $doctor->clinic_name) }}"
                                >
                            </div>
                            <div class="form-group">
                                <input type="password" placeholder="Password" class="form-control" name="password">
                            </div>
                            <div class="form-group">
                                <input type="text" placeholder="Affiliation" class="form-control" name="affiliation"
                                       value="{{ old('affiliation', $doctor->affiliation) }}"
                                >
                            </div>
                        </div>
                        <div class="col-sm-5 col-sm-offset-2 ">
                            <div class="form-group" style="font-size: 15px;line-height: 29px;">
                                <label class="main-color">ID</label>
                                <label>{{ $doctor->code or 'DO' . time() }}</label>
                                <input type="hidden" name="code" value="{{ $doctor->code or 'DO' . time() }}">
                            </div>
                            <div class="form-group">
                                <input type="text" placeholder="Speciality" class="form-control" name="speciality"
                                       value="{{ old('speciality', $doctor->speciality) }}"
                                >
                            </div>
                            <div class="form-group">
                                <input type="text" placeholder="Phone Number" class="form-control" name="phone_number"
                                       value="{{ old('phone_number', $doctor->phone_number) }}"
                                >
                            </div>
                            <div class="form-group">
                                <input type="text" placeholder="Clinic Phone Number" class="form-control" name="clinic_phone_number"
                                       value="{{ old('clinic_phone_number', $doctor->clinic_phone_number) }}"
                                >
                            </div>
                            <div class="form-group">
                                <input type="password" placeholder="Password Confirmation" class="form-control" name="password_confirmation">
                            </div>
                            <div class="form-group">
                                <input type="text" placeholder="Referred By" class="form-control" name="referred_by"
                                       value="{{ old('referred_by', $doctor->referred_by) }}"
                                >
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-4">
                                        <label class="secound-color" style="padding-left: 8px">Permission</label>
                                    </div>
                                    <div class="col-md-1">
                                        <label class="checkbox-inline">
                                            <input type="checkbox" name="medical"
                                                    {{ old('medical', $doctor->medical) ? 'checked' : '' }}
                                            > Medical
                                        </label>
                                    </div>
                                    <div class="col-md-5 col-sm-offset-2">
                                        <label class="checkbox-inline">
                                            <input type="checkbox" name="research"
                                                    {{ old('research', $doctor->research) ? 'checked' : '' }}
                                            > Research
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <button class="btn btn-sm btn-warning btn-block m-t-n-xs" type="submit">
                                <strong>Submit</strong>
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>