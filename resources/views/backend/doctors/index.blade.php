@extends('backend.layouts.default')

@section('content')
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h3 class="title m-t-sm">Doctors</h3>
                    <button class="btn btn-info btn-rounded" data-toggle="modal" data-target="#modalCreate">
                        Add New Doctor <i class="fa fa-plus-circle" aria-hidden="true" style="padding-left: 22px"></i>
                    </button>
                </div>
                <div class="ibox-content">
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered table-hover dataTables-example" >
                            <thead>
                            <tr class="frist">
                                <th>ID</th>
                                <th>Name</th>
                                <th>Email</th>
                                <th>Mobile</th>
                                <th>Speciality</th>
                                <th>Medical</th>
                                <th>Research</th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($doctors as $doctor)
                                <tr class="gradeX">
                                    <td>{{ $doctor->code }}</td>
                                    <td>
                                        <a href="{{ route('backend.doctors.show', $doctor->id) }}" target="_blank">
                                            {{ $doctor->name }}
                                        </a>
                                    </td>
                                    <td>{{ $doctor->email }}</td>
                                    <td>{{ $doctor->phone_number }}</td>
                                    <td>{{ $doctor->speciality }}</td>
                                    <td>
                                        @if($doctor->medical)
                                            <i class="fa fa-check" aria-hidden="true"></i>
                                        @endif
                                    </td>
                                    <td>
                                        @if($doctor->research)
                                            <i class="fa fa-check" aria-hidden="true"></i>
                                        @endif
                                    </td>
                                    <td>
                                        <button class="btn btn-danger btn-xs" data-toggle="modal" data-target="#modalDelete{{ $doctor->id }}"
                                        >
                                            <span class="glyphicon glyphicon-trash"></span>
                                        </button>&nbsp;&nbsp;
                                        <button class="btn btn-primary btn-xs" data-toggle="modal" data-target="#modalEdit{{ $doctor->id }}">
                                            <span class="glyphicon glyphicon-edit"></span>
                                        </button>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @include('backend.doctors._modal', [
        'doctor' => new App\Models\Doctor()
    ])

    @foreach($doctors as $doctor)
        @include('backend.doctors._modal', [
            'url' => route('backend.doctors.update', $doctor->id),
            'method' => 'PATCH',
            'id' => 'modalEdit' . $doctor->id
        ])
    @endforeach

    @foreach($doctors as $doctor)
        @include('backend.common._confirm_delete_modal', [
            'id' => $doctor->id,
            'url' => route('backend.doctors.destroy', $doctor->id)
        ])
    @endforeach
@endsection
