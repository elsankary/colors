@extends('backend.layouts.default')

@section('content')
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <div class="row m-b">
                        <div class="col-md-4 headline m-t">
                            <div style="font-size: 22px;">
                                <h3 class="secound-color" style="font-size: 22px; font-weight: bolder;">Dr:</h3>
                                <p class="main-color">{{ $doctor->name }}</p>
                            </div>
                            <div class="clearfix"></div>
                            <div class="m-t-sm">
                                <span class="secound-color">Mobile</span>
                                <span>{{ $doctor->phone_number }}</span>
                            </div>
                            <div class="m-t-sm">
                                <span class="secound-color">Email</span>
                                <span>{{ $doctor->email }}</span>
                            </div>
                            <div class="m-t-sm">
                                <span>
                                    @if($doctor->medical)
                                        <span class="label label-default">Medical</span>
                                    @endif

                                    @if($doctor->research)
                                        <span class="label label-default">Research</span>
                                    @endif
                                </span>
                            </div>
                        </div>
                        <div class="col-md-4 headline m-t">
                            <div style="font-size: 22px;">
                                <h3 class="secound-color" style="font-size: 22px; font-weight: bolder;">Clinic:</h3>
                                <p class="main-color">{{ $doctor->clinic_name }}</p>
                            </div>
                            <div class="m-t-sm">
                                <span class="secound-color">Clinic Number:</span>
                                <span>{{ $doctor->clinic_phone_number }}</span>
                            </div>
                            <div class="m-t-sm">
                                <span class="secound-color">National ID:</span>
                                <span>{{ $doctor->national_id }}</span>
                            </div>

                        </div>
                        <div class="col-md-4 headline m-t">
                            <div style="font-size: 22px; ">
                                <h3 class="secound-color" style="font-size: 22px; font-weight: bolder;">ID:</h3>
                                <p class="main-color">{{ $doctor->code }}</p>
                            </div>
                            <div class="m-t-sm">
                                <span class="secound-color">Affiliation</span>
                                <span>{{ $doctor->affiliation }}</span>
                            </div>
                            <div class="m-t-sm">
                                <span class="secound-color">Referred By:</span>
                                <span>{{ $doctor->referred_by }}</span>
                            </div>
                        </div>
                    </div>
                    <div class="line"></div>

                    <button class="btn btn-info btn-rounded" data-toggle="modal" data-target="#modalCreate">
                        &nbsp;&nbsp;Add New Patients <i class="fa fa-plus-circle" aria-hidden="true" style="padding-left: 22px"></i>
                    </button>
                </div>
                <div class="ibox-content">
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered table-hover dataTables-example">
                            <thead>
                            <tr class="frist">
                                <th>ID</th>
                                <th>Name</th>
                                <th>Mobile</th>
                                <th>National ID</th>
                                <th>Age</th>
                                <th>Analysis</th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($doctor->patients as $patient)
                                <tr class="gradeX">
                                    <td>{{ $patient->code }}</td>
                                    <td>
                                        <a href="{{ route('backend.patients.show', $patient->id) }}" target="_blank">
                                            {{ $patient->name }}
                                        </a>
                                    </td>
                                    <td>{{ $patient->phone_number }}</td>
                                    <td>{{ $patient->national_id }}</td>
                                    <td>{{ $patient->age }}</td>
                                    <td>{{ $patient->analyses->count() }}</td>

                                    <td>
                                        <button class="btn btn-danger btn-xs" data-toggle="modal" data-target="#modalDelete{{ $patient->id }}">
                                            <span class="glyphicon glyphicon-trash"></span>
                                        </button>&nbsp;&nbsp;
                                        <button class="btn btn-primary btn-xs" data-toggle="modal" data-target="#modalEdit{{ $patient->id }}">
                                            <span class="glyphicon glyphicon-edit"></span>
                                        </button>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @include('backend.patients._modal', [
        'patient' => new App\Models\Patient(),
        'doctorId' => $doctor->id
    ])

    @foreach($doctor->patients as $patient)
        @include('backend.patients._modal', [
            'url' => route('backend.patients.update', $patient->id),
            'method' => 'PATCH',
            'id' => 'modalEdit' . $patient->id,
            'doctorId' => $doctor->id
        ])
    @endforeach

    @foreach($doctor->patients as $patient)
        @include('backend.common._confirm_delete_modal', [
            'id' => $patient->id,
            'url' => route('backend.patients.destroy', $patient->id)
        ])
    @endforeach
@endsection