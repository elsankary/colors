<div id="{{ $id or 'modalCreate' }}" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h3 class="title">{{ $title or 'Add New Event' }}</h3>
            </div>
            <div class="modal-body">
                <form action="{{ $url or route('backend.events.store') }}" method="post">
                    {{ csrf_field() }}
                    @if(isset($method))
                        <input type="hidden" name="_method" value="{{ $method }}">
                    @endif
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <input type="text" placeholder="Name" class="form-control" name="name"
                                       value="{{ $event->name or '' }}"
                                >
                            </div>
                            <div class="input-group m-b">
                                <input type="text" class="form-control" placeholder="Category" name="category"
                                       value="{{ $event->category or '' }}"
                                >
                                <span class="input-group-addon"><span class="fa fa-search"></span></span>
                            </div>
                            <div class="form-group" id="data_1">
                                <div class=" input-group date">
                                    <span class="input-group-addon">
                                    <i class="fa fa-calendar"></i></span>
                                    <input type="text" class="form-control" name="from" placeholder="from"
                                           value="{{ $event->from ? $event->from->format('Y-m-d') : '' }}"
                                    >
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <input type="text" placeholder="External Link" class="form-control" name="external_link"
                                       value="{{ $event->external_link or '' }}"
                                >
                            </div>
                            <div class="form-group">
                                <input type="text" placeholder="location" class="form-control" name="location"
                                       value="{{ $event->location or '' }}"
                                >
                            </div>
                            <div class="form-group" id="data_1">
                                <div class="input-group date">
                                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                    <input type="text" class="form-control" name="to" placeholder="to"
                                           value="{{ $event->to ? $event->to->format('Y-m-d') : '' }}"
                                    >
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-12">
                            <div class="ibox float-e-margins">
                                <div class="ibox-content no-padding m-t">
                                    <textarea class="summernote" name="description" placeholder="Description">
                                        {{ $event->description or '' }}
                                    </textarea>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-12">
                            <button class="btn btn-sm btn-warning btn-block m-t-n-xs" type="submit">
                                <strong>Submit</strong>
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>