@extends('backend.layouts.default')

@section('content')
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h3 class="title m-t-sm">Events</h3>
                    <button class="btn btn-info btn-rounded" data-toggle="modal" data-target="#modalCreate">
                        Add New Event <i class="fa fa-plus-circle" aria-hidden="true" style="padding-left: 22px"></i>
                    </button>
                </div>
                <div class="ibox-content">
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered table-hover dataTables-example" >
                            <thead>
                            <tr class="frist">
                                <th class="hidden">id</th>
                                <th>Name</th>
                                <th>From</th>
                                <th>To</th>
                                <th>Location</th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($events as $event)
                                <tr class="gradeX">
                                    <td class="hidden"> {{ $event->id }}</td>
                                    <td>{{ $event->name }}</td>
                                    <td>{{ $event->from->format('Y-m-d') }}</td>
                                    <td>{{ $event->to->format('Y-m-d') }}</td>
                                    <td>{{ $event->location }}</td>
                                    <td>
                                        <button class="btn btn-danger btn-xs" data-toggle="modal" data-target="#modalDelete{{ $event->id }}"
                                        >
                                            <span class="glyphicon glyphicon-trash"></span>
                                        </button>&nbsp;&nbsp;
                                        <button class="btn btn-primary btn-xs" data-toggle="modal" data-target="#modalEdit{{ $event->id }}">
                                            <span class="glyphicon glyphicon-edit"></span>
                                        </button>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @include('backend.events._modal', [
        'event' => new App\Models\Event()
    ])

    @foreach($events as $event)
        @include('backend.events._modal', [
            'url' => route('backend.events.update', $event->id),
            'method' => 'PATCH',
            'id' => 'modalEdit' . $event->id
        ])
    @endforeach

    @foreach($events as $event)
        @include('backend.common._confirm_delete_modal', [
            'id' => $event->id,
            'url' => route('backend.events.destroy', $event->id)
        ])
    @endforeach
@endsection

@section('js')
    <script>
        function initMap() {
            var uluru = {lat: -25.363, lng: 131.044};
            var map = new google.maps.Map(document.getElementById('map'), {
                zoom: 4,
                center: uluru
            });
            var marker = new google.maps.Marker({
                position: uluru,
                map: map,
                draggable: true,
            });
        }
    </script>
    <script
            src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBeYAZwyoxzML1gIHChutO47EZJAd3yy0U&callback=initMap">
    </script>
@append
