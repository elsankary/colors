<!DOCTYPE html>
<html>
@include('backend.layouts.partials._head')
<body style="display: none;">
<div class="fakeloade"></div>
<div id="wrapper">
    @include('backend.layouts.partials._sidebar')

    <div id="page-wrapper" class="gray-bg dashbard-1">
        <div class="row ">
            <nav class="navbar navbar-static-top" role="navigation" style="margin-bottom: 0">
                <div class="navbar-header">
                    <a class="navbar-minimalize minimalize-styl-2 btn btn-primary" href="#">
                        <i class="fa fa-bars"></i>
                    </a>
                </div>
            </nav>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="wrapper wrapper-content">
                    @yield('content')
                </div>
            </div>
        </div>
    </div>
</div>
@include('backend.common._flash_message')
@include('backend.layouts.partials._scripts')
</body>
</html>