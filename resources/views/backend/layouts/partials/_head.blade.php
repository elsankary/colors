<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Color | @yield('pageTitle', 'Backend')</title>
    <link href="{{ asset('assets/backend/css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/backend/font-awesome/css/font-awesome.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/backend/css/plugins/dataTables/datatables.min.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/backend/css/animate.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/backend/css/plugins/summernote/summernote.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/backend/css/plugins/summernote/summernote-bs3.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/backend/css/style.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/backend/css/plugins/colorpicker/bootstrap-colorpicker.min.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/backend/css/plugins/cropper/cropper.min.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/backend/css/fakeLoader.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/vendor/toastr/toastr.css') }}" rel="stylesheet">
</head>