<nav class="navbar-default navbar-static-side" role="navigation"
     style="background-image: url({{ asset('assets/backend/img/header-profile.png') }}); background-repeat: no-repeat; background-size: cover;"
>
    <div class="sidebar-collapse">
        <ul class="nav metismenu" id="side-menu">
            <li class="nav-header">
                <div class="dropdown profile-element">
                        <span>
                        <img alt="image" src="{{ asset('assets/backend/img/logo.png') }}" style="max-width: 215px;"/>
                        </span>
                    <div class="row">
                        <div class="col-md-10">
                            <div class=" m-t-xs pull-right">
                                <span class="main-color font-bold">Welcome, </span>
                                <strong class="font-bold white">{{ $currentUser->name }}</strong>
                            </div>
                        </div>
                        <div class="col-sm-1">
                            <a href="{{ route('backend.auth.do_logout') }}" class="fa fa-sign-out btn-logout" style="font-size: 22px; margin-top: 4px"></a>
                        </div>
                    </div>
                </div>
                <div class="logo-element">
                    Colors
                </div>
            </li>
            <li class="m-t-sm {{ session('backend_active_group') == 'accounts' ? 'active' : '' }}">
                <a href="#" class="dropmenu">
                    <i class="fa fa-cog" aria-hidden="true"></i>
                    <span class="nav-label">Accounts</span>
                    <span class="fa arrow"></span>
                </a>
                <ul class="nav nav-second-level collapse sublink">
                    <li class="{{ session('backend_active_page') == 'admins' ? 'active' : '' }}">
                        <a href="{{ route('backend.admins.index') }}">Admins</a>
                    </li>
                    <li class="{{ session('backend_active_page') == 'doctors' ? 'active' : '' }}">
                        <a href="{{ route('backend.doctors.index') }}">Doctors</a>
                    </li>
                    <li class="{{ session('backend_active_page') == 'patients' ? 'active' : '' }}">
                        <a href="{{ route('backend.patients.index') }}">Patients</a>
                    </li>
                </ul>
            </li>
            <li class="m-t-sm {{ session('backend_active_group') == 'sections' ? 'active' : '' }}">
                <a href="{{ route('backend.sections.index') }}" class="dropmenu">
                    <i class="fa fa-sort-numeric-asc" aria-hidden="true"></i>
                    <span class="nav-label">Sections</span>
                </a>
            </li>
            <li class="m-t-sm {{ session('backend_active_group') == 'banners' ? 'active' : '' }}">
                <a href="{{ route('backend.banners.index') }}" class="dropmenu">
                    <i class="fa fa-plus" aria-hidden="true"></i>
                    <span class="nav-label">Banners</span>
                </a>
            </li>
            <li class="m-t-sm {{ session('backend_active_group') == 'orders' ? 'active' : '' }}">
                <a href="#" class="dropmenu">
                    <i class="fa fa-shopping-cart" aria-hidden="true"></i>
                    <span class="nav-label">Orders</span>
                    <span class="fa arrow"></span>
                </a>
                <ul class="nav nav-second-level collapse {{ session('backend_active_group') == 'orders' ? 'in' : '' }} sublink">
                    <li class="{{ session('backend_active_page') == 'calculator' ? 'active' : '' }}">
                        <a href="{{ route('backend.orders.calculator') }}">Calculator</a>
                    </li>
                    <li class="{{ session('backend_active_page') == 'patients' ? 'active' : '' }}">
                        <a href="{{ route('backend.orders.patients') }}">Patients</a>
                    </li>
                    <li class="{{ session('backend_active_page') == 'researches' ? 'active' : '' }}">
                        <a href="{{ route('backend.orders.researches') }}">Researches</a>
                    </li>
                </ul>
            </li>
            <li class="m-t-sm {{ session('backend_active_group') == 'events_and_news' ? 'active' : '' }}">
                <a href="#" class="dropmenu">
                    <i class="fa fa-newspaper-o" aria-hidden="true"></i>
                    <span class="nav-label">Events & News</span>
                    <span class="fa arrow"></span>
                </a>
                <ul class="nav nav-second-level collapse {{ session('backend_active_group') == 'events_and_news' ? 'in' : '' }} sublink">
                    <li class="{{ session('backend_active_page') == 'events' ? 'active' : '' }}">
                        <a href="{{ route('backend.events.index') }}">Events</a>
                    </li>
                    <li class="{{ session('backend_active_page') == 'news' ? 'active' : '' }}">
                        <a href="{{ route('backend.articles.index', ['type' => App\Models\Article::TYPE_NEWS]) }}">News</a>
                    </li>
                    <li class="{{ session('backend_active_page') == 'publications' ? 'active' : '' }}">
                        <a href="{{ route('backend.articles.index', ['type' => App\Models\Article::TYPE_PUBLICATIONS]) }}">Publications</a>
                    </li>
                </ul>
            </li>
            <li class="m-t-sm {{ session('backend_active_group') == 'pages' ? 'active' : '' }}">
                <a href="#" class="dropmenu">
                    <i class="fa fa-files-o" aria-hidden="true"></i>
                    <span class="nav-label">Pages</span>
                    <span class="fa arrow"></span>
                </a>
                <ul class="nav nav-second-level collapse {{ session('backend_active_group') == 'pages' ? 'in' : '' }} sublink">
                    <li class="{{ session('backend_active_page') == 'home' ? 'active' : '' }}">
                        <a href="{{ route('backend.pages.edit', App\Models\Page::KEY_HOME) }}">Home</a>
                    </li>
                    <li class="{{ session('backend_active_page') == 'about' ? 'active' : '' }}">
                        <a href="{{ route('backend.pages.edit', App\Models\Page::KEY_ABOUT) }}">About</a>
                    </li>
                    <li class="{{ session('backend_active_page') == 'contact-us' ? 'active' : '' }}">
                        <a href="{{ route('backend.pages.edit', App\Models\Page::KEY_CONTACT) }}">Contact us</a>
                    </li>
                </ul>
            </li>
        </ul>
        <div class="text-center m-t-70 hidden-sm">
            <p class="main-color font-bold">Copyrights © Colors 2017</p>
            <a href="http://www.maxeseg.com/" target="_blank"><p class="color-white font-bold">Maxes Business
                    Development </p></a>
        </div>
    </div>
</nav>