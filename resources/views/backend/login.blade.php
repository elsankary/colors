<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Color | login</title>
    <link href="{{ asset('assets/backend/css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/backend/font-awesome/css/font-awesome.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/backend/css/plugins/iCheck/custom.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/backend/css/animate.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/backend/css/style.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/backend/css/fakeLoader.css') }}" rel="stylesheet">
</head>
<body class="gray-bg" style="display: none;">
<div class="fakeloader"></div>
<div id="login"  style="background-image: url({{ asset('assets/backend/img/3.jpg') }}); background-size: cover; background-repeat: no-repeat; height: 100%; ">
    <div class="container">
        <div class="row" style="margin-top: 15%">

            <div class="col-md-2 col-md-offset-3">
                <img src="{{ asset('assets/backend/img/sign in.png') }}">
            </div>
            <div class="col-md-4">
                <form class="m-t" action="{{ route('backend.auth.show_login') }}" method="post">
                    {{ csrf_field() }}
                    <h1 >Medical System</h1>
                    <div class="form-group">
                        <input type="email" class="form-control" placeholder="Email Address" name="email" required>
                    </div>
                    <div class="form-group">
                        <input type="password" class="form-control" placeholder="Password" name="password" required>
                    </div>
                    <div class="form-group">
                        <div class="checkbox i-checks">
                            <label>
                                <input type="checkbox" name="remember_me">
                            </label>
                            <span style="color: #fff">Remember Me</span>
                        </div>
                    </div>
                    <button type="submit" class="btn btn-success block full-width m-b">Sign In</button>
                </form>
            </div>
        </div>
        <div class="row">
            <div class="col-md-5 col-md-offset-7" style=" position:fixed;bottom:4px;">
                <div class="row">
                    <div class="col-md-2">
                        <img src="{{ asset('assets/backend/img/footer.png') }}">
                    </div>
                    <div class="col-md-10">
                        <p class="main-color">Copyrights © Colors {{ date('Y') }}</p>
                        <p class="color-white" style="font-size: 10px">
                            Des. & Dev. :
                            <a href="http://maxeseg.com" target="_blank" class="copyrights-login">Maxes Business Development</a>
                        </p>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>
<script src="{{ asset('assets/backend/js/jquery-2.1.1.js') }}"></script>
<script src="{{ asset('assets/backend/js/bootstrap.min.js') }}"></script>
<script src="{{ asset('assets/backend/js/fakeLoader.min.js') }}"></script>
<script>
    $(document).ready(function(){
        $(".fakeloader").fakeLoader({
            timeToHide:1300,
            zIndex:99999999,
            bgColor:"#34495e",
            spinner:"spinner3"
        });
    });
    $(window).load(function() {
        $("body").fadeIn("slow");
    });
</script>
<!-- iCheck -->
<script src="{{ asset('assets/backend/js/plugins/iCheck/icheck.min.js') }}"></script>
<script>
    $(document).ready(function(){
        $('.i-checks').iCheck({
            checkboxClass: 'icheckbox_square-green',
            radioClass: 'iradio_square-green'
        });
    });
</script>
</body>
</html>
