<div id="modalAddFile" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h3 class="title m-l-0">{{ 'Add New File' }}</h3>
                <hr>
            </div>
            <div class="modal-body">
                <form action="{{ route('backend.analyses.add_file') }}" method="post" enctype="multipart/form-data">
                    {{ csrf_field() }}

                    <input type="hidden" name="analysis_id">

                    <div class="form-group">
                        <label for="pdf">PDF or images</label>
                        <input type="file" name="pdf" id="pdf" style="display: block">
                    </div>
                    <button class="btn btn-sm btn-warning btn-block m-t" type="submit">
                        <strong>Save</strong>
                    </button>
                </form>
            </div>
        </div>
    </div>
</div>