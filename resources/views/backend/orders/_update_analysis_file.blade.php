<div id="modalUpdateFile{{$order->orderable->id}}" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h3 class="title m-l-0">{{ 'Update File' }}</h3>
                <hr>
            </div>
            <div class="modal-body">
                <form action="{{ route('backend.analyses.add_file') }}" method="post" enctype="multipart/form-data">
                    {{ csrf_field() }}

                    <input type="hidden" name="analysis_id" value="{{ $order->orderable->id }}">

                    <div class="form-group" style="text-align:left ">
                        <label for="pdf">PDF Or Image</label>
                        <input type="file" name="pdf" id="pdf" style="display: block">
                        <br/>
                        @foreach($order->orderable->media as $file)
                            <a href="{{ $file->getUrl() }}" target="_blank" class="label label-default">
                                {{ $file->file_name }}
                            </a>
                        @endforeach
                    </div>
                    <button class="btn btn-sm btn-warning btn-block m-t" type="submit">
                        <strong>Save</strong>
                    </button>
                </form>
            </div>
        </div>
    </div>
</div>