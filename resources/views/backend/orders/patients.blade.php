@extends('backend.layouts.default')

@section('content')
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h3 class="title m-t-sm">
                        Patients
                    </h3>
                </div>
                <div class="ibox-content">
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered table-hover dataTables-example">
                            <thead>
                            <tr class="frist">
                                <th class="hidden">id</th>
                                <th>Date</th>
                                <th>Patient Name</th>
                                <th>Phone Number</th>
                                <th>Analysis</th>
                                <th>Download</th>
                                <th>Doctor</th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($orders as $order)
                                <tr class="gradeX">
                                    <td class="hidden"> {{ $order->id }}</td>
                                    <td>{{ $order->created_at->format('Y-m-d h:iA') }}</td>
                                    <td>
                                        @if($order->orderable && $order->orderable->patient)
                                            <a href="{{ route('backend.patients.show', $order->orderable->patient->id) }}"
                                               target="_blank">
                                                {{ $order->meta('patient_name') }}
                                            </a>
                                        @else
                                            {{ $order->meta('patient_name') }}
                                        @endif
                                    </td>
                                    <td>{{ $order->meta('patient_phone_number') }}</td>
                                    <td>{{ $order->meta('analysis_name') }}</td>
                                    <td>
                                        @if($order->orderable && !$order->orderable->media->isEmpty())
                                            @foreach($order->orderable->media as $item)
                                                <a href="{{ $item->getUrl() }}" target="_blank"
                                                   title="{{ $item->file_name }}">
                                                    <i class="fa fa-file-pdf-o fa-2x" aria-hidden="true"></i>
                                                </a>
                                            @endforeach
                                        @elseif($order->orderable)
                                            <button class="btn btn-bs-default btn-xs btn-add-file"
                                                    data-analysis-id="{{ $order->orderable->id }}">
                                                Add File
                                            </button>
                                        @endif
                                    </td>
                                    <td>
                                        @if($order->orderable && $order->orderable->patient && $order->orderable->patient->doctor)
                                            <a href="{{ route('backend.doctors.show', $order->orderable->patient->doctor->id) }}"
                                               target="_blank"
                                            >
                                                {{ $order->meta('doctor_name') }}
                                            </a>
                                        @else
                                            {{ $order->meta('doctor_name') }}
                                        @endif
                                    </td>
                                    <td>

                                        <button class="btn btn-danger btn-xs" data-toggle="modal" title="remove order"
                                                data-target="#modalDelete{{ $order->id }}">
                                            <span class="glyphicon glyphicon-trash"></span>
                                        </button>
                                        @if($order->orderable && !$order->orderable->media->isEmpty())
                                            <a class="btn btn-danger btn-xs" href="{{ route('backend.analyses.remove_file',$order->orderable->id) }}"
                                               title="remove analysis file">
                                                 Remove File
                                            </a>&nbsp;&nbsp;
                                            <button class="btn btn-primary btn-xs" data-toggle="modal" title="update analysis file"
                                                    data-target="#modalUpdateFile{{ $order->orderable->id }}">
                                                <span class="glyphicon glyphicon-edit"></span>
                                            </button>
                                            @include('backend.orders._update_analysis_file')
                                        @endif
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @include('backend.orders._add_analysis_file')

    @foreach($orders as $order)
        @include('backend.common._confirm_delete_modal', [
            'id' => $order->id,
            'url' => route('backend.orders.destroy', $order->id)
        ])
    @endforeach
@endsection

@section('js')
    <script>
        $('.btn-add-file').on('click', function () {
            $modal = $('#modalAddFile');
            $modal.find('input[name=analysis_id]').val($(this).data('analysis-id'));
            $modal.modal('show');
        });
    </script>
@append
