@extends('backend.layouts.default')

@section('content')
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h3 class="title m-t-sm">
                        Researches
                    </h3>
                </div>
                <div class="ibox-content">
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered table-hover dataTables-example" >
                            <thead>
                            <tr class="frist">
                                <th>Date</th>
                                <th>Order Details</th>
                                <th>Total Price</th>
                                <th>Doctor Name</th>
                                <th>Phone Number</th>
                                <th>Email</th>
                                <th>University</th>
                                <th>College</th>
                                <th>Notes</th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($orders as $order)
                                <tr class="gradeX">
                                    <td>{{ $order->created_at->format('Y-m-d h:iA') }}</td>
                                    <td>{{ $order->meta('sections_details') }}</td>
                                    <td>{{ $order->meta('total') }}</td>
                                    <td>
                                        @if($order->orderable)
                                            <a href="{{ route('backend.doctors.show', $order->orderable->id) }}" target="_blank">
                                                {{ $order->meta('name') }}
                                            </a>
                                        @else
                                            {{ $order->meta('name') }}
                                        @endif
                                    </td>
                                    <td>{{ $order->meta('phone_number') }}</td>
                                    <td>{{ $order->meta('email') }}</td>
                                    <td>{{ $order->meta('university') }}</td>
                                    <td>{{ $order->meta('college') }}</td>
                                    <td>
                                        @if(strlen($order->meta('notes')) > 30)
                                            <span data-toggle="tooltip" data-placement="top" title="{{ $order->meta('notes') }}">
                                                {{ str_limit($order->meta('notes'), 30) }}
                                            </span>
                                        @else
                                            {{ str_limit($order->meta('notes'), 30) }}
                                        @endif
                                    </td>
                                    <td>
                                        <button class="btn btn-danger btn-xs" data-toggle="modal" data-target="#modalDelete{{ $order->id }}"
                                        >
                                            <span class="glyphicon glyphicon-trash"></span>
                                        </button>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @foreach($orders as $order)
        @include('backend.common._confirm_delete_modal', [
            'id' => $order->id,
            'url' => route('backend.orders.destroy', $order->id)
        ])
    @endforeach
@endsection

@section('js')
    <script>
        $('[data-toggle="tooltip"]').tooltip();
    </script>
@append
