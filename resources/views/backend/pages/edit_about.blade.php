@extends('backend.layouts.default')

@section('content')
    <div id="app">
        <form method="post" action="{{ route('backend.pages.update', ['key' => App\Models\Page::KEY_ABOUT]) }}"
              enctype="multipart/form-data"
        >
            {{ csrf_field() }}
            <input type="hidden" name="_method" value="PATCH">

            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h3 class="title m-b-50 m-l-0">
                        Videos
                        <button type="button" class="btn btn-info btn-rounded" v-on:click="createVideo">&nbsp;&nbsp;
                            Add New Video
                            <i class="fa fa-plus-circle" aria-hidden="true" style="padding-left: 22px"></i>
                        </button>
                    </h3>
                </div>
            </div>

            <div class="videos">
                <div class="inputs">
                    @foreach($videos as $video)
                        <input type="hidden" value="{{ $video->shortUrl() }}">
                    @endforeach
                </div>
                <div class="clearfix">
                    <app-video v-for="(video, index) in videos" :video="video" :i="index"></app-video>
                </div>
            </div>

            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h3 class="title m-b-50 m-l-0">
                        Images
                        <button type="button" class="btn btn-info btn-rounded" v-on:click="createImage">&nbsp;&nbsp;
                            Add New Images
                            <i class="fa fa-plus-circle" aria-hidden="true" style="padding-left: 22px"></i>
                        </button>
                    </h3>
                </div>
            </div>

            <div class="images" data-existing-images="{{ $images->toJson() }}">
                <div class="inputs hidden"></div>
                <div class="clearfix">
                    <app-image v-for="(image, index) in images" :image="image" :i="index"></app-image>
                </div>
            </div>

            <div class="ibox float-e-margins">
                <div class="ibox-title" style="padding: 0"></div>

                <div class="ibox-content no-padding m-t">
                    <textarea name="body" placeholder="Body" class="summernote">{!! $body !!}</textarea>
                </div>
            </div>

            <div class="text-right">
                <button class="btn btn-info btn-rounded" type="submit">Submit</button>
            </div>
        </form>
    </div>

    @include('backend.pages.modals._add_image')
    @include('backend.pages.modals._add_video')

    @include('backend.pages.partials._styles')
    @include('backend.pages.partials._scripts')
@endsection
