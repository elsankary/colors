@extends('backend.layouts.default')

@section('content')
    <form action="{{ route('backend.pages.update', ['key' => App\Models\Page::KEY_CONTACT]) }}" method="post">
        <input type="hidden" name="_method" value="PATCH">
        {{ csrf_field() }}

        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h3 class="title m-b-50" style="    margin-left: -7px;">Contact information</h3>
            </div>
        </div>

        <div class="form-group">
            <label>Phone</label>
            <input name="phone_number" class="form-control" value="{{ old('phone_number', $phone_number) }}">
        </div>

        <div class="form-group">
            <label>Address</label>
            <textarea name="address" class="resizable_textarea form-control"
                      rows="4"
            >{{ old('address', $address) }}</textarea>
        </div>

        <div class="form-group">
            <label>Email</label>
            <input name="email" class="form-control" type="email" value="{{ old('email', $email) }}">
        </div>

        <div class="form-group">
            <label>Facebook</label>
            <input name="facebook" class="form-control" type="text" value="{{ old('facebook', $facebook) }}">
        </div>
        <div class="form-group">
            <label>Linkedin</label>
            <input name="linkedin" class="form-control" type="text" value="{{ old('linkedin', $linkedin) }}">
        </div>

        <div class="form-group">
            <label>Youtube</label>
            <input name="youtube" class="form-control" type="text" value="{{ old('youtube', $youtube) }}">
        </div>

        <div class="form-group">
            <label>Map</label>
            <div class="row">
                <div class="col-md-6">
                    <input name="latitude" class="form-control" type="text" placeholder="Latitude" value="{{ old('latitude', $latitude) }}">
                </div>
                <div class="col-md-6">
                    <input name="longitude" class="form-control" type="text" placeholder="Longitude" value="{{ old('longitude', $longitude) }}">
                </div>
            </div>
        </div>

        <hr>

        <textarea name="description" class="summernote">{!! $description !!}</textarea>

        <hr>

        <div class="text-right">
            <button type="submit" class="btn btn-primary">Submit</button>
        </div>
    </form>
@endsection