@extends('backend.layouts.default')

@section('content')
    <div id="app">
        <form action="{{ route('backend.pages.update', ['key' => App\Models\Page::KEY_HOME]) }}" method="post">
            <input type="hidden" name="_method" value="PATCH">
            {{ csrf_field() }}
            
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h3 class="title m-b-50 m-l-0">Videos</h3>
                </div>
            </div>

            <div class="videos">
                <div class="inputs">
                    <input type="hidden" value="{{ $video->shortUrl() }}">
                </div>
                <div class="clearfix">
                    <app-video v-for="(video, index) in videos" :video="video" :i="index" :no-delete="true"></app-video>
                </div>
            </div>

            <div class="ibox float-e-margins">
                <div class="ibox-content no-padding m-t">
                    <textarea name="body" placeholder="Body" class="summernote">{!! $body !!}</textarea>
                </div>
            </div>

            <div class="text-right">
                <button type="submit" class="btn btn-primary">Submit</button>
            </div>
        </form>
    </div>

    @include('backend.pages.modals._add_video')

    @include('backend.pages.partials._styles')
    @include('backend.pages.partials._scripts')
@endsection