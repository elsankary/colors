<div id="add-image" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h3 class="title m-b-lg">Add Image</h3>
            </div>
            <div class="modal-body">
                <div class="thumbnail" style="background-image: url({{ asset('assets/backend/img/placeholder.png') }})"></div>

                <input type="hidden" id="preview" value="">

                <label class="btn btn-info">
                    Upload Photo <input type="file" name="images[]" onchange="app.previewImage(this)">
                </label>
                <button class="btn btn-primary" onclick="app.saveImage()">Save</button>
            </div>
        </div>
    </div>
</div>