<div id="add-video" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h3 class="title m-b-lg">Add video</h3>
            </div>
            <div class="modal-body">
                <input type="hidden" class="edit" value="">
                <div class="form-group">
                    <label>Add URL (Youtube)</label>
                    <input type="text" class="form-control embed-url" placeholder="Youtube Video URL">
                </div>
                <div class="text-right">
                    <button class="btn btn-info btn-add-video" type="button" onclick="app.saveVideo()">
                        Save changes
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>