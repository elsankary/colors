<template id="imageTemplate">
    <div class="file-box">
        <div class="file">
            <span class="corner"></span>

            <img class="img-responsive center-block" :src="image.url">

            <div class="file-name">
                <a href="#" class="btn btn-white btn-sm" v-on:click="remove()">
                    <i class="fa fa-trash"></i> Delete
                </a>
            </div>
        </div>
    </div>
</template>

<template id="videoTemplate">
    <div class="file-box">
        <input type="hidden" name="videos[]" :value="video.embedUrl">
        <div class="file">
            <a href="#">
                <div class="embed-responsive embed-responsive-16by9">
                    <iframe :src="video.embedUrl" frameborder="0"
                            webkitallowfullscreen mozallowfullscreen allowfullscreen
                    ></iframe>
                </div>

                <div class="file-name">
                    <a href="#" class="btn btn-white btn-sm btn-delete" v-on:click="remove()" v-if="!noDelete">
                        <i class="fa fa-trash"></i> Delete
                    </a>
                    <a href="#" class="btn btn-white btn-sm btn-edit" v-on:click="edit()">
                        <i class="fa fa-edit"></i> Edit
                    </a>
                </div>
            </a>
        </div>
    </div>
</template>

@section('js')
    <script src="https://unpkg.com/vue"></script>
    <script>
        var $videoModal = $('#add-video');
        var $imageModal = $('#add-image');

        var app = new Vue({
            el: '#app',
            data: {
                videos: [],
                images: []
            },
            created: function () {
                var that = this;

                $('.videos .inputs input').each(function () {
                    that.videos.push(that.parseVideo($(this).val()));
                });

                $.each($('.images').data('existing-images'), function (i, image) {
                    that.images.push({url: image.url, id: image.id});
                });
            },
            methods: {
                createVideo: function () {
                    $videoModal.find('input').val('');
                    $videoModal.modal('show');
                },
                editVideo: function (i) {
                    $videoModal.find('input.edit').val(i);
                    $videoModal.find('input.embed-url').val(this.videos[i].embedUrl);
                    $videoModal.modal('show');
                },
                saveVideo: function () {
                    var i = $videoModal.find('input.edit').val();

                    if (i) {
                        app.$set(app.videos, i, this.parseVideo($videoModal.find('.embed-url').val()));
                    } else {
                        this.videos.push(this.parseVideo($videoModal.find('.embed-url').val()));
                    }

                    $videoModal.modal('hide');
                },
                parseVideo: function (url) {
                    var videoId = url.match(/(?:https?:\/{2})?(?:w{3}\.)?youtu(?:be)?\.(?:com|be)(?:\/watch\?v=|\/)([^\s&]+)/)[1];

                    return {
                        id: videoId,
                        embedUrl: 'https://www.youtube.com/embed/' + videoId
                    };
                },
                createImage: function () {
                    $imageModal.find('input').val('');
                    $imageModal.modal('show');
                },
                saveImage: function () {
                    console.log($imageModal.find('input[type=file]').clone().val());

                    $('.images .inputs').append(
                        $imageModal.find('input[type=file]').clone()
                    );

                    this.images.push({
                        url: $imageModal.find('#preview').val()
                    });

                    $imageModal.modal('hide');
                },
                previewImage: function (input) {
                    var reader = new FileReader();

                    reader.onload = function (e) {
                        $imageModal.find('input#preview').val(e.target.result);
                        $imageModal.find('.thumbnail').css('background-image', 'url(' + e.target.result + ')');
                    };

                    reader.readAsDataURL(input.files[0]);
                }
            },
            components: {
                'app-video': {
                    template: '#videoTemplate',
                    props: ['video', 'i', 'noDelete'],
                    methods: {
                        remove: function () {
                            app.$delete(app.videos, this.i);
                        },
                        edit: function () {
                            app.editVideo(this.i);
                        }
                    }
                },
                'app-image': {
                    template: '#imageTemplate',
                    props: ['image', 'i'],
                    methods: {
                        remove: function () {
                            if (this.image.id) {
                                $.ajax({
                                    url: '/backend/media/' + this.image.id,
                                    method: 'DELETE',
                                    success: function (response) {
                                        console.log(response)
                                    }
                                });
                            }

                            app.$delete(app.images, this.i);
                        }
                    }
                }
            }
        });
    </script>
@append