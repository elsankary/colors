<style>
    #add-image .thumbnail {
        width: 200px;
        height: 200px;
        background: #f1eff1 center center no-repeat;
        background-size: contain;
    }

    .image-widget {
        width: 200px;
        border: 1px solid #e7eaec;
    }

    .image-widget:hover {
        background-color: #9c9fa1;
    }

    .image-widget  input[type=file] {
        width: 0.1px;
        height: 0.1px;
        opacity: 0;
        overflow: hidden;
        position: absolute;
        z-index: -1;
    }


    .image-widget  input[type=file] + label {
        display: inline-block;
        width: 100%;
        height: 150px;
        background-color: #fff;
        background-size: contain;
        margin-bottom: 0;
    }

    .image-widget .buttons {
        display: inline-block;
        width: 100%;
        padding: 10px;
        background-color: #f8f8f8;
    }

    .image-widget  input[type=file]:focus + label,
    .image-widget  input[type=file] + label:hover {
        background-color: #9c9fa1;
    }

    .image-widget  input[type=file] + label {
        cursor: pointer;
    }

    .image-widget  input[type=file]:focus + label {
        outline: 1px dotted #000;
        outline: -webkit-focus-ring-color auto 5px;
    }

    .image-widget  input[type=file] + label * {
        pointer-events: none;
    }
</style>