<div id="{{ $id or 'modalCreate' }}" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h3 class="title">{{ $title or 'Add New Patient' }}</h3>
            </div>
            <div class="modal-body">
                <form action="{{ $url or route('backend.patients.store') }}" method="post">
                    {{ csrf_field() }}
                    @if(isset($method))
                        <input type="hidden" name="_method" value="{{ $method }}">
                    @endif
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <input type="text" placeholder="Name" class="form-control" name="name"
                                       value="{{ old('name', $patient->name) }}"
                                >
                            </div>
                            <div class="form-group">
                                <input type="text" placeholder="Phone Number" class="form-control" name="phone_number"
                                       value="{{ old('phone_number', $patient->phone_number) }}"
                                >
                            </div>
                            <div class="form-group">
                                <input type="number" placeholder="Age" class="form-control" name="age"
                                       value="{{ old('age', $patient->age) }}"
                                >
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group" style="font-size: 14px; line-height: 12px;">
                                <label class="main-color">ID</label>
                                <label>{{ $patient->code or 'PA' . time() }}</label>
                                <input type="hidden" name="code" value="{{ $patient->code or 'PA' . time() }}">
                            </div>

                            <div class="form-group">
                                <input type="text" placeholder="National Id" class="form-control" name="national_id"
                                       value="{{ old('national_id', $patient->national_id) }}"
                                >
                            </div>

                            <div class="form-group">
                                <input type="password" placeholder="Password" class="form-control" name="password">
                            </div>
                        </div>
                        @if(isset($doctorId))
                            <input type="hidden" name="doctor_id" value="{{ $doctorId }}">
                        @else
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="doctorId">Doctor</label>
                                    <select name="doctor_id" id="doctorId" class="form-control">
                                        <option value="0">No Doctor</option>
                                        @foreach($doctors as $doctor)
                                            <option value="{{ $doctor->id }}" {{ old('doctor_id', $patient->doctor_id ) ? 'selected' : '' }}>
                                                {{ $doctor->name }}
                                            </option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        @endif
                        <div class="col-md-12">
                            <button class="btn btn-sm btn-warning btn-block m-t-n-xs" type="submit">
                                <strong>Submit</strong>
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>