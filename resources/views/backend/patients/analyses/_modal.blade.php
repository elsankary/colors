<div id="{{ $id or 'modalCreate' }}" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h3 class="title m-l-0">{{ $title or 'Add New Analysis' }}</h3>
                <hr>
            </div>
            <div class="modal-body">
                <form action="{{ $url or route('backend.analyses.store') }}" method="post"
                      enctype="multipart/form-data">
                    @if(isset($method))
                        <input type="hidden" name="_method" value="{{ $method }}">
                    @endif

                    {{ csrf_field() }}

                    <input type="hidden" name="patient_id" value="{{ $patient->id }}">

                    <div class="form-group">
                        <div class="form-group" style="font-size: 15px;line-height: 29px;">
                            <label class="main-color">Code</label>
                            <label>{{ $analysis->code or 'AN' . time() }}</label>
                            <input type="hidden" name="code" value="{{ $analysis->code or 'AN' . time() }}">
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="name">Name</label>
                        <input type="text" name="name" value="{{ old('name', $analysis->name) }}" id="name"
                               class="form-control" required
                        >
                    </div>

                    <div class="form-group">
                        <label for="status">Status</label>
                        <select name="status" id="status" class="form-control">
                            <option value="pending" {{ old('name', $analysis->status) ==  'pending' ? 'selected' : '' }}>Pending</option>
                            <option value="done" {{ old('name', $analysis->status) ==  'done' ? 'selected' : '' }}>Done</option>
                        </select>
                    </div>

                    <div class="form-group">
                        <label for="pdf">PDF Or Image</label>
                        <input type="file" name="pdf" id="pdf" style="display: block">
                    </div>
                    @foreach($analysis->media as $file)
                        <a href="{{ $file->getUrl() }}" target="_blank" class="label label-default">
                            {{ $file->file_name }}
                        </a>
                    @endforeach

                    <button class="btn btn-sm btn-warning btn-block m-t" type="submit">
                        <strong>Save</strong>
                    </button>
                </form>
            </div>
        </div>
    </div>
</div>