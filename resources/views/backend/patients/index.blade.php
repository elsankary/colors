@extends('backend.layouts.default')

@section('content')
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h3 class="title m-t-sm">Patients</h3>
                    <button class="btn btn-info btn-rounded" data-toggle="modal" data-target="#modalCreate">
                        Add New Patient <i class="fa fa-plus-circle" aria-hidden="true" style="padding-left: 22px"></i>
                    </button>
                </div>
                <div class="ibox-content">
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered table-hover dataTables-example">
                            <thead>
                            <tr class="frist">
                                <th>Id</th>
                                <th>Name</th>
                                <th>Mobile</th>
                                <th>National Id</th>
                                <th>Age</th>
                                <th>Analysis</th>
                                <th>Doctor</th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($patients as $patient)
                                <tr class="gradeX">
                                    <td>{{ $patient->code }}</td>
                                    <td>
                                        <a href="{{ route('backend.patients.show', $patient->id) }}" target="_blank">
                                            {{ $patient->name }}
                                        </a>
                                    </td>
                                    <td>{{ $patient->phone_number }}</td>
                                    <td>{{ $patient->national_id }}</td>
                                    <td>{{ $patient->age }}</td>
                                    <td>{{ $patient->analyses->count() }}</td>
                                    <td>
                                        @if($patient->doctor)
                                            <a href="{{ route('backend.doctors.show', $patient->doctor->id) }}">
                                                {{ $patient->doctor->name }}
                                            </a>
                                        @endif
                                    </td>
                                    <td>
                                        <button class="btn btn-danger btn-xs" data-toggle="modal" data-target="#modalDelete{{ $patient->id }}">
                                            <span class="glyphicon glyphicon-trash"></span>
                                        </button>&nbsp;&nbsp;
                                        <button class="btn btn-primary btn-xs" data-toggle="modal" data-target="#modalEdit{{ $patient->id }}">
                                            <span class="glyphicon glyphicon-edit"></span>
                                        </button>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @include('backend.patients._modal', [
        'patient' => new App\Models\Patient()
    ])

    @foreach($patients as $patient)
        @include('backend.patients._modal', [
            'url' => route('backend.patients.update', $patient->id),
            'method' => 'PATCH',
            'id' => 'modalEdit' . $patient->id
        ])
    @endforeach

    @foreach($patients as $patient)
        @include('backend.common._confirm_delete_modal', [
            'id' => $patient->id,
            'url' => route('backend.patients.destroy', $patient->id)
        ])
    @endforeach
@endsection
