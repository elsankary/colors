@extends('backend.layouts.default')

@section('content')
    <div class="ibox float-e-margins">
        <div class="ibox-title">
            <div class="row m-b">
                <div class="col-md-4 headline">
                    <div style="font-size: 22px;">
                        <h3 class="secound-color" style="font-size: 22px; font-weight: bolder;">Patient/</h3>
                        <p class="main-color">{{ $patient->name }}</p>
                    </div>
                    <div class="clearfix"></div>
                    <div class="m-t-sm" style="font-size: 22px;">
                        <h3 class="secound-color" style="font-size: 22px; font-weight: bolder;">Mobile/</h3>
                        <p class="main-color">{{ $patient->phone_number }}</p>
                    </div>
                </div>
                <div class="col-md-4 headline">
                    <div style="font-size: 22px;">
                        <h3 class="secound-color" style="font-size: 22px; font-weight: bolder;">National ID/</h3>
                        <p class="main-color">{{ $patient->national_id }}</p>
                    </div>
                    <div class="m-t-sm" style="font-size: 22px;">
                        <h3 class="secound-color" style="font-size: 22px; font-weight: bolder;">Age/</h3>
                        <p class="main-color">{{ $patient->age }}</p>
                    </div>
                </div>
                <div class="col-md-4 headline">
                    <div style="font-size: 22px;">
                        <h3 class="secound-color" style="font-size: 22px; font-weight: bolder;">Code/</h3>
                        <p class="main-color">{{ $patient->code }}</p>
                    </div>
                </div>
            </div>

            @if($patient->doctor)
                <div class="headline">
                    <div style="font-size: 22px;">
                        <h3 class="secound-color" style="font-size: 22px; font-weight: bolder;">Doctor/</h3>
                        <p class="main-color">{{ $patient->doctor->name }}</p>
                    </div>
                </div>
            @endif

            <div class="line"></div>

            <h3 class="title m-t-sm">
                Analysis
                <button class="btn btn-info btn-rounded" data-toggle="modal" data-target="#modalCreate">
                    Add New Analysis <i class="fa fa-plus-circle" aria-hidden="true" style="padding-left: 22px"></i>
                </button>
            </h3>
        </div>
        <div class="ibox-content">
            <div class="table-responsive">
                <table class="table table-striped table-bordered table-hover dataTables-example">
                    <thead>
                    <tr class="frist">
                        <th>Date</th>
                        <th>Analysis Name</th>
                        <th>Analysis ID</th>
                        <th>status</th>
                        <th>PDF File</th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($patient->analyses as $analysis)
                        <tr class="gradeX">
                            <td>{{ $analysis->created_at->format('Y-m-d h:iA') }}</td>
                            <td>{{ $analysis->name }}</td>
                            <td>{{ $analysis->code }}</td>
                            <td>{{ $analysis->status }}</td>
                            <td class="text-center">
                                @foreach($analysis->media as $item)
                                    <a href="{{ $item->getUrl() }}" target="_blank">
                                        <i class="fa fa-file-pdf-o fa-2x" aria-hidden="true"></i>
                                    </a>
                                @endforeach
                            </td>
                            <td>
                                <button class="btn btn-danger btn-xs" data-toggle="modal" data-target="#modalDelete{{ $analysis->id }}">
                                    <span class="glyphicon glyphicon-trash"></span>
                                </button>&nbsp;&nbsp;
                                <button class="btn btn-primary btn-xs" data-toggle="modal" data-target="#modalEdit{{ $analysis->id }}">
                                    <span class="glyphicon glyphicon-edit"></span>
                                </button>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>

    @include('backend.patients.analyses._modal', [
        'analysis' => new App\Models\Analysis()
    ])

    @foreach($patient->analyses as $analysis)
        @include('backend.patients.analyses._modal', [
            'url' => route('backend.analyses.update', $analysis->id),
            'method' => 'PATCH',
            'id' => 'modalEdit' . $analysis->id
        ])
    @endforeach

    @foreach($patient->analyses as $analysis)
        @include('backend.common._confirm_delete_modal', [
            'id' => $analysis->id,
            'url' => route('backend.analyses.destroy', $analysis->id)
        ])
    @endforeach
@endsection