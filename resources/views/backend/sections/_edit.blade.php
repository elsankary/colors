@extends('backend.layouts.default')
@section('content')
    <div class="ibox float-e-margins">
        <div class="ibox-title">
            <div class="row m-b">
                <div class="col-md-4 headline">
                    <div style="font-size: 22px;">
                        <h3 class="secound-color" style="font-size: 22px; font-weight: bolder;">Section name/</h3>
                        <p class="main-color">{{ $section->name }}</p>
                    </div>
                    <div class="clearfix"></div>
                    <div class="m-t-sm" style="font-size: 22px;">
                        <h3 class="secound-color" style="font-size: 22px; font-weight: bolder;">Section Type/</h3>
                        <p class="main-color">{{ $section->present()->type }}</p>
                    </div>
                </div>
                <div class="col-md-4 headline">
                    <div style="font-size: 22px;">
                        <h3 class="secound-color" style="font-size: 22px; font-weight: bolder;">Section Price/</h3>
                        <p class="main-color">{{ $section->present()->priceFormatted }}</p>
                    </div>
                    @if($section->parent_id != null)
                        <div class="m-t-sm" style="font-size: 22px;">
                            <h3 class="secound-color" style="font-size: 22px; font-weight: bolder;">Section Parent/</h3>
                            <p class="main-color">{{ $section->present()->parentsList }}</p>
                        </div>
                    @endif
                </div>
            </div>

            <div class="line"></div>

            <h3 class="title m-t-sm">
                Edit Section-{{ $section->name }}
            </h3>
            <div class="line"></div>
        </div>

        <div class="ibox-content">
            <form action="{{ route('backend.sections.update',$section->id) }}" method="post">
                {{ csrf_field() }}

                <input type="hidden" value="{{ $section->type }}" name="type">

                @include('backend.sections._update_tree')

                <div class="form-group">
                    <label for="name">Name</label>
                    <input type="text" name="name" value="{{ old('name', $section->name) }}" id="name"
                           class="form-control" required
                    >
                </div>

                <div class="form-group">
                    <label for="price">Price</label>
                    <input type="text" name="price" value="{{ old('price', $section->price) }}" id="price"
                           class="form-control" required
                    >
                </div>

                <div class="form-group">
                    <label for="description">Description</label>
                    <div class="ibox float-e-margins">
                        <div class="ibox-content no-padding m-t">
                            <textarea name="description" placeholder="Description" class="summernote">{{ old('description', $section->description) }}</textarea>
                        </div>
                    </div>
                </div>

                <button class="btn btn-sm btn-warning btn-block m-t" type="submit">
                    <strong>Save</strong>
                </button>
            </form>
        </div>

    </div>
@endsection

