{{ csrf_field() }}

<div class="form-group">
    <label for="type">Type</label>
    <select name="type" id="type" class="form-control">
        <option value="medical"
            {{ old('type', $section->type) == \App\Models\Section::TYPE_MEDICAL || empty($section->type) ? 'selected' : ''}}>Medical
        </option>
        <option value="research"
                {{ old('type', $section->type) == \App\Models\Section::TYPE_RESEARCH ? 'selected' : '' }}> Research
        </option>

    </select>
</div>

@include('backend.sections._tree')

<div class="form-group">
    <label for="name">Name</label>
    <input type="text" name="name" value="{{ old('name', $section->name) }}" id="name"
           class="form-control" required
    >
</div>

<div class="form-group">
    <label for="price">Price</label>
    <input type="text" name="price" value="{{ old('price', $section->price) }}" id="price"
           class="form-control" required
    >
</div>

<div class="form-group">
    <label for="description">Description</label>
    <textarea name="description" id="description" class="form-control"
              data-ckeditor
    >{{ old('description', $section->description) }}</textarea>
</div>

<button class="btn btn-sm btn-warning btn-block m-t" type="submit">
    <strong>Save</strong>
</button>