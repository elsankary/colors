<div id="{{ $id or 'modalCreate' }}" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h3 class="title">{{ $title or 'Add New Section' }}</h3>
            </div>
            <div class="modal-body">
                <form action="{{ $url or route('backend.sections.store') }}" method="post">
                    {{ csrf_field() }}
                    @if(isset($method))
                        <input type="hidden" name="_method" value="{{ $method }}">
                    @endif

                    <div class="form-group">
                        <label for="type">Type</label>
                        <select name="type" id="type" class="form-control type">
                            <option value="medical" selected>Medical</option>
                            <option value="research"> Research</option>
                        </select>
                    </div>

                    @include('backend.sections._tree')

                    <div class="form-group">
                        <label for="name">Name</label>
                        <input type="text" name="name" id="name"
                               class="form-control" required
                        >
                    </div>

                    <div class="form-group">
                        <label for="price">Price</label>
                        <input type="text" name="price" id="price"
                               class="form-control" required
                        >
                    </div>

                    <div class="form-group">
                        <label for="description">Description</label>
                        <div class="ibox float-e-margins">
                            <div class="ibox-content no-padding m-t">
                                <textarea name="description" placeholder="Description" class="summernote">{{ old('description', $section->description) }}</textarea>
                            </div>
                        </div>

                        {{--<label for="description">Description</label>--}}
                        {{--<textarea name="description" id="description" class="form-control"--}}
                                  {{--data-ckeditor>{{ old('description', $section->description) }}</textarea>--}}
                    </div>

                    <button class="btn btn-sm btn-warning btn-block m-t" type="submit">
                        <strong>Save</strong>
                    </button>
                </form>
            </div>
        </div>
    </div>
</div>