<div class="form-group">
    <label for="parentId">Parent</label>
    <select name="parent_id" id="parentId" class="form-control">
        <option value="0">:: No Parent ::</option>
        @foreach($tree as $sectionId => $sectionName)
                <option value="{{ $sectionId }}">{{ $sectionName }}</option>
        @endforeach
    </select>
</div>