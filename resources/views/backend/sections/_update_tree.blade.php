<div class="form-group">
    <label for="parentId">Parent</label>
    <select name="parent_id" id="parentId" class="form-control">
        <option value="0">:: No Parent ::</option>
        @foreach($tree as $sectionId => $sectionName)
            @if(!(trim(preg_replace('/-/', '', $sectionName)) ==  $section->name))
                <option value="{{ $sectionId }}" {{ $sectionId ==  $section->parent_id ? 'selected' : '' }}>{{ $sectionName }}</option>
            @endif
        @endforeach
    </select>
</div>