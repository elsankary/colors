@extends('backend.layouts.default')

@section('content')
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h3 class="title m-t-sm">Sections</h3>
                    <button class="btn btn-info btn-rounded" data-toggle="modal" data-target="#modalCreate">
                        Add New Section <i class="fa fa-plus-circle" aria-hidden="true" style="padding-left: 22px"></i>
                    </button>
                </div>
                <div class="ibox-content">
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered table-hover dataTables-example">
                            <thead>
                            <tr class="frist">
                                <th>ID</th>
                                <th>Name</th>
                                <th>Parents</th>
                                <th>Type</th>
                                <th>Price</th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($sections as $section)
                                <tr class="gradeX">
                                    <td>{{ $section->id }}</td>
                                    <td>{{ $section->name }}</td>
                                    <td>{{ $section->present()->parentsList }}</td>
                                    <td>{{ $section->present()->type }}</td>
                                    <td>{{ $section->present()->priceFormatted }}</td>
                                    <td>
                                        <button class="btn btn-danger btn-xs" data-toggle="modal"
                                                data-target="#modalDelete{{ $section->id }}"
                                        >
                                            <span class="glyphicon glyphicon-trash"></span>
                                        </button>&nbsp;&nbsp;
                                        <a class="btn btn-primary btn-xs" href="{{ route('backend.sections.edit',$section->id) }}">
                                            <span class="glyphicon glyphicon-edit"></span>
                                        </a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @include('backend.sections._modal', [
        'section' => new App\Models\Section()
    ])

    @foreach($sections as $section)
        @include('backend.common._confirm_delete_modal', [
            'id' => $section->id,
            'url' => route('backend.sections.destroy', $section->id)
        ])
    @endforeach
@endsection

@section('js')
    <script type="text/javascript">

        $(document).ready(function () {
            $('#type').change(function () {
                loadTree($('#type').val());
            });

        });

        function loadTree(type) {
            $.ajax({
                url: 'sections/getTree',
                data: {
                    type: type
                },
                success: function (data) {
                    $("#parentId").html('');
                    $("#parentId").html(data);
                }
            });
        }
    </script>
@append