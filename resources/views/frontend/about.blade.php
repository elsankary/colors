@extends('frontend.layouts.default')

@section('css')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/baguettebox.js/1.8.1/baguetteBox.min.css">
    <link rel="stylesheet" href="{{ asset('assets/frontend/css/gallery-clean.css') }}">
@append

@section('content')
    <section>
        <div id="cover" style="background-image: url({{ asset('assets/frontend/img/About.jpg') }});">
            <div class="overla">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12 m-t-120">
                            <h1 class="white-color font-light-42" style="margin-bottom: 0">About</h1>
                            <span class="white-color font-30">Colors Medical Laboratories</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section id="about">
        <div class="container">
            <div class="row">
                <div class="col-md-12 m-t-40">
                    <p style="color: #5e5e5e">{!! $page->content('body') !!}</p>
                </div>
            </div>
        </div>
    </section>
    <section>
        <div class="container">
            <!-- img -->
            <div class="collect">
                <div class="carsoul">
                    <h1 class="main-color" style="
                    margin-bottom: 0;">Photos</h1>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="carousel slide media-carousel" id="media">
                                <div class="carousel-inner tz-gallery">
                                    @foreach($page->media->chunk(4) as $x => $chunk)
                                        <div class="item {{ $x == 1 ? 'active' : '' }}">
                                            <div class="row text-center">
                                                @foreach($chunk as $image)
                                                    <div class="col-md-3 col-sm-12 m-b-10">
                                                        <a class="lightbox" style="display: block; position: inherit; background: none;" href="{{ $image->getUrl() }}">
                                                            <img src="{{ $image->getUrl() }}" alt="Coast">
                                                        </a>
                                                    </div>
                                                @endforeach
                                            </div>
                                        </div>
                                    @endforeach
                                </div>
                                <a class="left carousel-control" href="#media" role="button" data-slide="prev">
                                    <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                                    <span class="sr-only">Previous</span>
                                </a>
                                <a class="right carousel-control" href="#media" role="button" data-slide="next">
                                    <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                                    <span class="sr-only">Next</span>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- video -->
            <div class="collect">
                <div class="carsoul">
                    <h1 class="main-color" style="
                    margin-bottom: 0;">Videos</h1>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="carousel slide media-carousel" id="media2">
                                <div class="carousel-inner">
                                    @foreach($videos->chunk(4) as $chunk)
                                        <div class="item  active">
                                            <div class="row text-center">
                                                @foreach($chunk as $video)
                                                    <div class="col-md-3 col-xs-12   m-b-10 videolightbox">
                                                        <a href="#lightbox" class="lightbox-toggle"
                                                           style="max-width: 250px; max-height: 190px; background-image: url({{ $video->thumbnail() }})"
                                                           data-lightbox-type="video"
                                                           data-lightbox-content="{{ $video->embedUrl() }}"
                                                        >
                                                            <i class="fa fa-play-circle-o" aria-hidden="true"></i>
                                                        </a>
                                                    </div>
                                                @endforeach
                                            </div>
                                        </div>
                                    @endforeach
                                </div>
                                <a class="left carousel-control" href="#media2" role="button" data-slide="prev">
                                    <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                                    <span class="sr-only">Previous</span>
                                </a>
                                <a class="right carousel-control" href="#media2" role="button" data-slide="next">
                                    <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                                    <span class="sr-only">Next</span>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    @include('frontend.layouts.partials._footer')
@endsection

@section('js')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/baguettebox.js/1.8.1/baguetteBox.min.js"></script>
    <script>
        baguetteBox.run('.tz-gallery');
    </script>
@append