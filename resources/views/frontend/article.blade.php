@extends('frontend.layouts.default')

@section('content')
    <section>
        <div id="cover" style="background-image: url({{ asset('assets/frontend/img/Publicationpage.jpg') }});">
            <div class="overla">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12 m-t-140">
                            <h1 class="white-color bold font-42">{{ $article->title }}</h1>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section>
        <div class="container m-b-40 m-t-20">
            <div style="height: 240px">{!! $article->body !!}</div>
        </div>
    </section>
    @include('frontend.layouts.partials._footer')
@append