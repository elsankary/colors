<div id="request" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h2 class="main-color font-27 bold">Request</h2>
            </div>
            <div class="modal-body">
                <form action="/calculator/request" method="post" id="formRequest">
                    {{ csrf_field() }}

                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <input type="text" placeholder="Name" class="form-control" name="name"
                                       value="{{ auth('doctor')->check() ? auth('doctor')->user()->name : '' }}"
                                >
                            </div>
                        </div>
                        <div class="col-sm-6 ">
                            <div class="form-group">
                                <input type="number" placeholder="Mobile" class="form-control" name="phone_number"
                                       value="{{ auth('doctor')->check() ? auth('doctor')->user()->phone_number : '' }}"
                                >
                            </div>
                            <div class="form-group">
                                <input type="text" placeholder="University" class="form-control" name="university">
                            </div>
                        </div>
                        <div class="col-sm-6 ">
                            <div class="form-group">
                                <input type="email" placeholder="E-mail" class="form-control" name="email"
                                       value="{{ auth('doctor')->check() ? auth('doctor')->user()->email : '' }}"
                                >
                            </div>
                            <div class="form-group">
                                <input type="text" placeholder="College" class="form-control" name="college">
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <input type="text" placeholder="Department" class="form-control" name="department">
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <textarea class="form-control" placeholder="Notes" rows="5" id="notes" name="notes"></textarea>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <button class="btn btn-sm btn-purple btn-block m-t-n-xs" type="submit">
                                <strong>Submit</strong>
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>