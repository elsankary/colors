<div class="widget widget_has_radio_checkbox">
    <div class="block">
        <ul class="list-unstyled">
            @foreach($tree as $item)
                <li>
                    <div class="parent">
                        <label>
                            <input type="checkbox" value="{{$item->child_flag==0? $item->price:0 }}" class="input-price i-check parent" id="{{ $item->id }}">
                            <i></i> + {{ $item->name }}
                        </label>
                    </div>
                    <ul>
                        @foreach($item->children as $subItem)
                            <li class="list-unstyled">
                                <label>
                                    <input type="checkbox" value="{{ $subItem->price }}" class="input-price i-check child{{ $item->id }}" id="{{ $subItem->id }}">
                                    <i></i> {{ $subItem->name }}
                                </label>
                            </li>
                        @endforeach
                    </ul>
                </li>
            @endforeach
        </ul>
    </div>
</div>
