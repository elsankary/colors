@extends('frontend.layouts.default')

@section('content')
    <section>
        <div id="cover" style="background-image: url({{ asset('assets/frontend/img/Calculator.jpg') }});">
            <div class="overla">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12 m-t-140 text-center">
                            <h1 class="white-color ">Calculators</h1>
                            <p class="white-color font-25 " style="font-family: MyriadPro-Regular;">
                                Services request and price estimate
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section id="calculator">
        <div class="container m-b-40 m-t-20">
            <div>
                <h2 class="main-color font-27" style="font-family: MyriadPro-Bold;">Please select the section</h2>
            </div>
            <div class="row">
                <div class="col-md-12 section_price">
                    <div class="row">
                        <div class="col-md-3">
                            <div class="form-group">
                                <select class="form-control" id="type">
                                    <option value="{{ \App\Models\Section::TYPE_MEDICAL }}">Medical</option>
                                    <option value="{{ \App\Models\Section::TYPE_RESEARCH }}">Research</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-9">
                            <span style="font-size: 18px; font-family: MyriadPro-Regular;">Choose Medical or Research</span>
                        </div>
                    </div>

                    <div class="line"></div>

                    <div class="tree-wrapper">
                        @include('frontend.calculator._tree')
                    </div>

                    <div class="row m-t-60">
                        <div class="col-md-12 font-27 regular">
                            <p class="gray-color" style="margin: 0;">Result</p>
                        </div>
                        <div class="clearfix"></div>
                        <div class="col-md-2 font-27 " style="    margin-top: -15px;">
                            <span class="price regular subtotal">0</span>
                            <span style="color:#5fc8f9" class="font-27 regular"> EGP</span>
                        </div>
                        <div class="col-md-3">
                            <button type="button" class="btn btn-purple" data-toggle="modal" data-target="#request">
                                Request
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    @include('frontend.calculator._modal')
@endsection

@section('js')
    <script>
        $('body').on('ifChanged', '.input-price', function () {
            check();
            updateSubTotal();
        });

        $('#type').on('change', function () {
            $.ajax({
                url: '/calculator/tree',
                data: {
                    type: $(this).val()
                },
                success: function (response) {
                    var wrapper = $('.tree-wrapper');
                    wrapper.html(response);
                    setICkeck();
                }
            });
        });

        $('#formRequest').on('submit', function () {
            var $this = $(this);
            getSelectedSections().each(function () {
                $this.append(
                    '<input type="hidden" name="sections[]" value="' + $(this).attr('id') + '">'
                );
            });
        });

        setICkeck();

        function setICkeck() {
            $('.tree-wrapper .i-check').iCheck({
                checkboxClass: 'icheckbox_square-green',
                radioClass: 'iradio_square-green'
            });
        }

        function updateSubTotal() {
            var subtotal = 0;
            getSelectedSections().each(function () {
                subtotal += parseInt($(this).val());
            });

            $('.subtotal').text(subtotal);
        }

        function check() {
//            getUnSelectedSections().each(function () {
//                if($(this).hasClass('parent')){
//                    var id = $(this).attr('id');
//                    $('.child' + id).prop("checked", false);
//                    setICkeck();
//                }
//            });
            getSelectedSections().each(function () {
                if($(this).hasClass('parent')){
                    var id = $(this).attr('id');
                    $('.child' + id).prop("checked", true);
                    setICkeck();
                }
            });
        }

        function getSelectedSections() {
            return $('.tree-wrapper').find('.input-price:checked');
        }

//        function getUnSelectedSections() {
//            return $('.tree-wrapper').find('.input-price:checkbox:not(:checked)');
//        }

    </script>
@append