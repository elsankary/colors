@extends('frontend.layouts.default')

@section('content')
    <section>
        <div id="cover"  style="background-image: url({{ asset('assets/frontend/img/Contactus.jpg') }});">
            <div class="overla">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12 m-t-160">
                            <h1 class="white-color font-42 bold">Contact Us</h1>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section>
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h2 class="main-color font-27 bold">Contact Info</h2>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-6">
                    <table class="table table-info">
                        <tr>
                            <th>Phone</th>
                            <td>{{ $page->content('phone_number') }}</td>
                        </tr>
                        <tr>
                            <th>Email</th>
                            <td>{{ $page->content('email') }}</td>
                        </tr>
                        <tr>
                            <th>Address</th>
                            <td>{{ $page->content('address') }}</td>
                        </tr>
                    </table>
                </div>
                <div class="col-sm-6">
                    <div id="googleMap" style="height: 300px;"></div>
                </div>
            </div>

            {!! $page->content('description') !!}
        </div>
    </section>

    @include('frontend.layouts.partials._footer')
@endsection

@section('js')
    <script>
        function initMap() {
            new google.maps.Map(document.getElementById('googleMap'), {
                center: {lat: {{ $page->content('latitude') }}, lng: {{ $page->content('longitude') }}},
                zoom: 8
            });
        }
    </script>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBMxG_k7dxrIpXI5i1dFf5jORXN9w3cywk&callback=initMap"
            async defer
    ></script>
@append
