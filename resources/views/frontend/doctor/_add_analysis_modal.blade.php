<div class="modal fade" id="addAnalysis" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">×</button>
                <h3 class="main-color font-25 bold">Add Analysis</h3>
            </div>
            <div class="modal-body">
                <form action="{{ url('/doctor/add-analysis') }}" method="post">
                    {{ csrf_field() }}

                    <input type="hidden" name="patient_id">

                    <div class="form-group">
                        <input type="text" placeholder="Name" class="form-control" name="name">
                    </div>

                    <button class="btn btn-sm btn-purple btn-block" type="submit">
                        <strong>Add</strong>
                    </button>
                </form>
            </div>
        </div>
    </div>
</div>