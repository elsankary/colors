<div class="modal fade" id="addPatient" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">×</button>
                <h3 class="main-color font-25 bold">Add Patient</h3>
            </div>
            <div class="modal-body">
                <form action="{{ url('/doctor/add-patient') }}" method="post">
                    {{ csrf_field() }}
                    <div class="form-group">
                        <input type="text" placeholder="Name" class="form-control" name="name">
                    </div>
                    <div class="form-group">
                        <input type="text" placeholder="National ID" class="form-control" name="national_id">
                    </div>
                    <div class="form-group">
                        <input type="number" placeholder="Age" class="form-control" name="age">
                    </div>
                    <div class="form-group">
                        <input type="text" placeholder="Phone Number" class="form-control" name="phone_number">
                    </div>
                    <div class="form-group">
                        <input type="text" placeholder="Analysis" class="form-control" name="analysis">
                    </div>

                    <button class="btn btn-sm btn-purple btn-block" type="submit">
                        <strong>Add</strong>
                    </button>
                </form>
            </div>
        </div>
    </div>
</div>