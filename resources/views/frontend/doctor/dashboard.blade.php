@extends('frontend.layouts.default')

@section('content')
    <div id="cover" style="background-image: url({{ asset('assets/frontend/img/doctor.jpg') }});">
        <div class="overla">
            <div class="container">
                <div class="row">
                    <div class="col-md-12 m-t-140 text-center white-color">
                        <h1 class="bold font-42">{{ auth('doctor')->user()->name }}</h1>
                        <a href="/doctor/logout" class="btn btn-yollow">Logout</a>
                        <button type="button" class="btn btn-purple" data-toggle="modal" data-target="#addPatient">
                            Add Patient
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <section id="doctorDashboard">
        <div class="container">
            <h2>Patients</h2>
            <div class="table-responsive">
                <table class="table table-bordered table-hover table-vertical-middle">
                    <tr>
                        <th>Id</th>
                        <th>Name</th>
                        <th>Mobile</th>
                        <th>National Id</th>
                        <th>Age</th>
                        <th>Analysis</th>
                        <th></th>
                    </tr>
                    @foreach($patients as $patient)
                        <tr>
                            <td>{{ $patient->code }}</td>
                            <td>{{ $patient->name }}</td>
                            <td>{{ $patient->phone_number }}</td>
                            <td>{{ $patient->national_id }}</td>
                            <td>{{ $patient->age }}</td>
                            <td>
                                @foreach($patient->analyses as $analysis)
                                    @if($analysis->media->isEmpty())
                                        <span href="javascript:void(0)" class="label label-default label-block" style="margin-bottom: 5px;">
                                            {{ $analysis->name }}
                                        </span>
                                    @else
                                        @foreach($analysis->media as $pdf)
                                            <a href="{{ $pdf->getUrl() }}" target="_blank"
                                               class="btn btn-success btn-xs btn-block"
                                               style="margin-bottom: 5px;"
                                            >
                                                {{ $analysis->name }}
                                            </a>
                                        @endforeach
                                    @endif
                                @endforeach
                            </td>
                            <td>
                                <button href="#" class="btn btn-bs-default btn-xs btn-add-analysis"
                                        data-patient-id="{{ $patient->id }}"
                                >
                                    Add Analysis
                                </button>
                            </td>
                        </tr>
                    @endforeach
                </table>
            </div>
        </div>
    </section>

    @include('frontend.doctor._add_patient_modal')
    @include('frontend.doctor._add_analysis_modal')
@endsection

@section('js')
    <script>
        $('.btn-add-analysis').on('click', function (e) {
            e.preventDefault();

            var modal = $('#addAnalysis');
            modal.find('input[name=patient_id]').val($(this).data('patient-id'));
            modal.modal('show');
        });
    </script>
@append