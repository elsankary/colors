@extends('frontend.layouts.default')

@section('content')
    <section>
        <div id="cover" style="background-image: url({{ asset('assets/frontend/img/doctor.jpg') }});">
            <div class="overla">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12 m-t-140 text-center white-color">
                            <h1 class="bold font-42">Doctor Department</h1>

                            @if(auth('doctor')->guest())
                                <p class="font-22">Please login to prview your patients</p>
                                <button type="button" class="btn btn-yollow" data-toggle="modal" data-target="#login">Login</button>
                                <button type="button" class="btn btn-purple" data-toggle="modal" data-target="#register">
                                    Register
                                </button>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section id="category">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="tabs-container">
                        <div class="nav nav-tabs block text-center">
                            @foreach($tree as $item)
                                <a data-toggle="tab" href="#tab-{{ $item->id }}" aria-expanded="false"
                                   class="btn btn-danger btn-rounded btn-outline category"
                                >{{ $item->name }}</a>
                            @endforeach
                        </div>
                        <div class="tab-content text-center">
                            @foreach($tree as $item)
                                <div id="tab-{{ $item->id }}" class="tab-pane">
                                    <div class="panel-body">
                                        @foreach($item->children as $subItem)
                                            <a class="btn btn-danger btn-rounded btn-outline suncat">{{ $subItem->name }}</a>
                                        @endforeach
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>

            @if(auth('doctor')->guest())
                <div class="modal fade" id="login" role="dialog">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">×</button>
                                <h3 class="main-color font-25 bold">Login</h3>
                            </div>
                            <div class="modal-body">
                                <form action="{{ url('/doctor/login') }}" method="post">
                                    {{ csrf_field() }}
                                    <div class="form-group">
                                        <input type="text" placeholder="Email" class="form-control" name="email">
                                    </div>
                                    <div class="form-group">
                                        <input type="password" placeholder="Password" class="form-control" name="password">
                                    </div>
                                    <button class="btn btn-sm btn-purple btn-block" type="submit">
                                        <strong>Login</strong>
                                    </button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            @endif

            <div class="modal fade" id="register" role="dialog">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">×</button>
                            <h3 class="main-color font-25 bold">Register</h3>
                        </div>
                        <div class="modal-body">
                            <form action="{{ url('/doctor/register') }}" method="post">
                                {{ csrf_field() }}
                                <div class="row">
                                    <div class="col-sm-5 ">
                                        <div class="form-group">
                                            <input type="text" placeholder="Name" class="form-control" name="name">
                                        </div>
                                        <div class="form-group">
                                            <input type="text" placeholder="National ID" class="form-control" name="national_id">
                                        </div>
                                        <div class="form-group">
                                            <input type="email" placeholder="E-mail" class="form-control" name="email">
                                        </div>
                                        <div class="form-group">
                                            <input type="text" placeholder="Clinic Name" class="form-control" name="clinic_name">
                                        </div>
                                        <div class="form-group">
                                            <input type="password" placeholder="Password" class="form-control" name="password">
                                        </div>
                                        <div class="form-group">
                                            <input type="password" placeholder="Confirm Password" class="form-control"
                                                   name="password_confirmation"
                                            >
                                        </div>
                                    </div>
                                    <div class="col-sm-5 col-sm-offset-2 ">
                                        <div class="form-group">
                                            <select name="speciality" class="form-control">
                                                <option value="Speciality 1">Speciality 1</option>
                                                <option value="Speciality 2">Speciality 2</option>
                                                <option value="Speciality 3">Speciality 3</option>
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <input type="text" placeholder="Mobile" class="form-control" name="phone_number">
                                        </div>
                                        <div class="form-group">
                                            <input type="text" placeholder="Clinic Number" class="form-control" name="clinic_phone_number">
                                        </div>
                                    </div>
                                    <div class="col-md-12 m-t-10">
                                        <button class="btn btn-sm btn-purple btn-block" type="submit">
                                            <strong>Register</strong>
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection