@extends('frontend.layouts.default')

@section('content')
    <section>
        <div id="cover" style="background-image: url({{ asset('assets/frontend/img/Events.jpg') }});">
            <div class="overla">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12 m-t-140">
                            <h1 class="white-color text-center bold font-42">Events</h1>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section id="event">
        <div class="container m-b-40 m-t-20">
            <div>
                <h2 class="main-color font-30 bold">Incoming Events</h2>
            </div>
            <div class="row ">
                @foreach($events as $event)
                    <div class="col-md-5 m-t-20">
                        <div class="block">
                            <h3 class="bold font-22">{{ $event->name }}</h3>
                            <div class="row regular">
                                <div class="col-md-6">
                                    <span>Data From</span>
                                    <span>{{ $event->from->format('Y-m-d') }}</span>
                                </div>
                                <div class="col-md-6">
                                    <span>Data To</span>
                                    <span>{{ $event->to->format('Y-m-d') }}</span>
                                </div>
                                <div class="col-md-6">
                                    <p>{{ $event->location }}</p>
                                </div>

                                <div class="col-md-6">
                                    <p>{{ $event->category }}</p>
                                </div>

                                <div class="col-md-12">
                                    {!! $event->description !!}
                                </div>

                                @if($event->external_link)
                                    <div class="col-md-12 text-right">
                                        <a href="{{ $event->external_link }}" target="_blank" class="btn btn-default">More</a>
                                    </div>
                                @endif
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </section>

    @include('frontend.layouts.partials._footer')
@endsection