@extends('frontend.layouts.default')

@section('content')
    <section id="department">
        <div>
            <div class="container-fluid">
                <div class="row m-t-70">

                    <div class="col-md-4 overlay red depaertment1 showhim">
                        <div class="ok text-center">
                            <img src="{{ asset('assets/frontend/img/medicail.png') }}" style="margin-top: 50%">
                            <h1 class="white-color">Medical</h1>
                        </div>
                        <div class="showme text-center">
                            <div class="row">
                                <div class="col-md-12 " style="margin-top: 50%">
                                    <h3 class="white-color font-light-42">You are?</h3>
                                </div>
                                <div class="col-md-3 col-md-offset-3 col-sm-6">
                                    <a href="/doctor"><img src="{{ asset('assets/frontend/img/medical-doctor.png') }}"></a>
                                    <h3 class="white-color">Doctor</h3>
                                </div>
                                <div class="col-md-3  col-sm-6">
                                    <a href="/patient"><img
                                                src="{{ asset('assets/frontend/img/medical-patient.png') }}"></a>
                                    <h3 class="white-color">Patient</h3>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 depaertment2 overlay deop2">
                        <div class="row">
                            <div class="col-md-12  text-center" style="margin-top: 50%">
                                <a href="/research">
                                    <img src="{{ asset('assets/frontend/img/search.png') }}" class="center-block">
                                </a>
                                <h1 class="white-color">Re-Search</h1>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 overlay red depaertment3  deop3 showhim">
                        <div class=" ok text-center">
                            <img src="{{ asset('assets/frontend/img/animail&food.png') }}" style="margin-top: 50%">
                            <h1 class="white-color">Animal &amp; Food</h1>
                        </div>
                        <div class="showme text-center">
                            <div class="row">
                                <div class="col-md-12 " style="margin-top: 50%">
                                    <h2 class="white-color font-light-42">Choose</h2>
                                </div>
                                <div class="col-md-3 col-md-offset-3">
                                    <a href="#"><img src="{{ asset('assets/frontend/img/animail.png') }}"></a>
                                    <h3 class="white-color">Animal</h3>
                                </div>
                                <div class="col-md-3">
                                    <a href="#"><img src="{{ asset('assets/frontend/img/food.png') }}"></a>
                                    <h3 class="white-color">Food</h3>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <footer id="footer_home">
        <div style="background-image: url({{ asset('assets/frontend/img/cover-footer.jpg') }}); border-top: 7px solid #ddd">
            <div class="container">
                <div class="row m-t-40 m-b-30">
                    <div class="col-md-4 videolightbox">
                        <a href="#lightbox" class="lightbox-toggle"
                           data-lightbox-type="video"
                           data-lightbox-content="{{ $video->embedUrl() }}"
                           style="background-image: url({{ $video->thumbnail() }})"
                        >
                            <i class="fa fa-play-circle-o" aria-hidden="true"></i>
                        </a>
                    </div>
                    <div class="col-md-8 white-color">
                        <div class="m-b-10 ">
                            <h1 style="display: inline; font-size: 38px">Colors</h1>
                            <h3 style="display: inline;">Medical Laboratories</h3>
                        </div>
                        <p>{!! $page->content('body') !!}</p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12" style="border-top: 2px solid #ddd">
                        <div class="row m-t-10 m-b-10">
                            <div class="col-md-8 m-t-10">
                                <ul class="list-inline ">
                                    <li class="list-inline-item white-color">
                                        <a href="/" class="white-color">Home</a>
                                    </li>
                                    <li class="list-inline-item">
                                        <a href="/about" class="white-color">About</a>
                                    </li>
                                    <li class="list-inline-item">
                                        <a href="/news" class="white-color">News &amp; Events</a>
                                    </li>
                                    <li class="list-inline-item">
                                        <a href="/calculator" class="white-color">Calculator</a>
                                    </li>
                                    <li class="list-inline-item">
                                        <a href="/contact" class="white-color">Contact us</a>
                                    </li>
                                </ul>
                            </div>
                            <div class="col-md-4">
                                <div class="pull-right white-color">
                                    <span>Copyrights © Colors {{ date('Y') }}</span> <br>
                                    <span>Des. & Dev. : <a href="http://maxeseg.com" target="_blank" class="copyrights">Maxes Business Development</a> </span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </footer>
@endsection