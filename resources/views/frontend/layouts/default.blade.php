<!DOCTYPE html>
<html lang="en">
@include('frontend.layouts.partials._head')
<body>
@include('frontend.layouts.partials._header')

@yield('content')

@if(auth('doctor')->check() || auth('patient')->check())
    @include('frontend.layouts.partials._logged_in')
@endif

@include('frontend.layouts.partials._flash_message')
@include('frontend.layouts.partials._scripts')
</body>
</html>