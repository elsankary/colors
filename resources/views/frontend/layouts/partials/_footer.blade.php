<footer>
    <div class="container">
        <div class="row m-t-10 m-b-10" style="border-top: 1px solid #000;">
            <div class="col-md-9 m-t-10">
                <ul class="list-inline m-t-10">
                    <li class="list-inline-item white-color">
                        <a href="/" class="white-color">Home</a>
                    </li>
                    <li class="list-inline-item">
                        <a href="/about" class="white-color">About</a>
                    </li>
                    <li class="list-inline-item">
                        <a href="/news" class="white-color">News &amp; Events</a>
                    </li>
                    <li class="list-inline-item">
                        <a href="/calculator" class="white-color">Calculator</a>
                    </li>
                    <li class="list-inline-item">
                        <a href="/contact" class="white-color">Contact Us</a>
                    </li>
                </ul>
            </div>
            <div class="col-md-3 m-t-10">
                <div class="pull-right">
                    <span>Copyrights © Colors {{ date('Y') }}</span><br>
                    <span>Des. & Dev. : <a href="http://maxeseg.com" target="_blank" class="copyrights">Maxes Business Development</a> </span>
                </div>
            </div>
        </div>
    </div>
</footer>