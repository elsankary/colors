<header class="header">
    <nav class="navbar navbar-default navbar-fixed-top" role="navigation"  id="custom-nav">
        <div class="container" style="margin-top: 12px;">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbarCollapse">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="/">
                    <img class="" src="{{ asset('assets/frontend/img/logo.png') }}" >
                </a>
            </div>
            <div class="collapse navbar-collapse navbarCollapse navbarcenter">
                <ul class="nav navbar-nav ">
                    <li class="{{ session('frontend_active_page') == 'home' ? 'active' : '' }}">
                        <a href="/">Home</a>
                    </li>
                    <li class="{{ session('frontend_active_page') == 'about' ? 'active' : '' }}">
                        <a href="/about">About</a>
                    </li>
                    <li class="dropdown {{ in_array(session('frontend_active_page'), ['news', 'events', 'publications']) ? 'active' : '' }}">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            News &amp; Events <b class="caret"></b>
                        </a>
                        <ul class="dropdown-menu">
                            <li>
                                <a href="/news">News</a>
                            </li>
                            <li class="divider"></li>
                            <li>
                                <a href="/events">Events</a>
                            </li>
                            <li class="divider"></li>
                            <li>
                                <a href="/publications">Publications</a>
                            </li>
                        </ul>
                    </li>
                    <li class="{{ session('frontend_active_page') == 'calculator' ? 'active' : '' }}">
                        <a href="/calculator">Calculator</a>
                    </li>
                    <li class="{{ session('frontend_active_page') == 'contact' ? 'active' : '' }}">
                        <a href="/contact">Contact us</a>
                    </li>
                </ul>
                <ul class="nav navbar-nav navbar-right social-nav model-3d-0">
                    <li>
                        <a href="{{ $shared['facebook'] }}" class="facebook" target="_blank">
                            <div class="front"><i class="fa fa-facebook"></i></div>
                            <div class="back"><i class="fa fa-facebook"></i></div>
                        </a>
                    </li>
                    <li>
                        <a href="{{ $shared['linkedin'] }}" class="linkedin" target="_blank">
                            <div class="front"><i class="fa fa-linkedin"></i></div>
                            <div class="back"><i class="fa fa-linkedin"></i></div>
                        </a>
                    </li>
                    <li>
                        <a href="{{ $shared['youtube'] }}" class="youtube" target="_blank">
                            <div class="front"><i class="fa fa-youtube-play"></i></div>
                            <div class="back"><i class="fa fa-youtube-play"></i></div>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
</header>