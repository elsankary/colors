<div id="loggedIn">
    <p>
        @if(auth('doctor')->check())
            Welcome, {{ auth('doctor')->user()->name }}.
            <a href="/doctor/dashboard">Dashboard</a> | <a href="/doctor/logout">Logout</a>
        @elseif(auth('patient')->check())
            Welcome, {{ auth('patient')->user()->name }}.
            <a href="/patient/dashboard">Dashboard</a> | <a href="/patient/logout">Logout</a>
        @endif
    </p>
</div>