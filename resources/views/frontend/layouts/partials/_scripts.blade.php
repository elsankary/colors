<script src="{{ asset('assets/frontend/js/jquery-2.1.1.js') }}"></script>
<script src="{{ asset('assets/frontend/js/bootstrap.min.js') }}"></script>
<script src="{{ asset('assets/frontend/js/wow.min.js') }}"></script>
<script src="{{ asset('assets/frontend/js/main.js') }}"></script>
<script src="{{ asset('assets/frontend/js/lightbox.js') }}"></script>
<script src="{{ asset('assets/frontend/js/iCheck/icheck.min.js') }}"></script>
<script src="{{ asset('assets/vendor/toastr/toastr.min.js') }}"></script>

@yield('js')