@extends('frontend.layouts.default')

@section('content')
    <div id="cover" style="background-image: url({{ asset('assets/frontend/img/Patien.jpg') }});">
        <div class="overla">
            <div class="container">
                <div class="row">
                    <div class="col-md-12 m-t-140 text-center white-color">
                        <h1 class="bold font-42">{{ auth('patient')->user()->name }}</h1>
                        <a href="/patient/logout" class="btn btn-yollow">Logout</a>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <section id="patientDashboard">
        <div class="container">
            <h2>Analyses</h2>
            <div class="table-responsive">
                <table class="table table-bordered table-hover table-vertical-middle">
                    <tr>
                        <th>Date</th>
                        <th>Name</th>
                        <th>Code</th>
                        <th>Status</th>
                        <th>Download</th>
                    </tr>
                    @foreach($analyses as $analysis)
                        <tr>
                            <td>{{ $analysis->created_at->format('Y-m-d h:iA') }}</td>
                            <td>{{ $analysis->name }}</td>
                            <td>{{ $analysis->code }}</td>
                            <td>{{ $analysis->status }}</td>
                            <td>
                                @foreach($analysis->media as $item)
                                    <a href="{{ $item->getUrl() }}" class="btn btn-success btn-xs btn-block" target="_blank">Download</a>
                                @endforeach
                            </td>
                        </tr>
                    @endforeach
                </table>
            </div>
        </div>
    </section>
@endsection