@extends('frontend.layouts.default')

@section('content')
    <section>
        <div id="cover" style="background-image: url({{ asset('assets/frontend/img/Patien.jpg') }});">
            <div class="overla">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12 m-t-140 text-center white-color">
                            <h1 class="bold font-42">Patient Department</h1>

                            @if(auth('patient')->guest())
                                <p class="font-22">Please login to preview your analyses.</p>
                                <button type="button" style="  font-family: MyriadPro-Light;     font-size: 22px;
                        " class="btn btn-yollow light"
                                        data-toggle="modal" data-target="#login"
                                >
                                    Login
                                </button>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    @if(auth('doctor')->guest())
        <div class="modal fade" id="login" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">×</button>
                        <h3 class="main-color font-25 bold">Login</h3>
                    </div>
                    <div class="modal-body">
                        <form action="{{ url('/patient/login') }}" method="post">
                            {{ csrf_field() }}
                            <div class="form-group">
                                <input type="text" placeholder="Phone Number" class="form-control" name="phone_number">
                            </div>
                            <div class="form-group">
                                <input type="password" placeholder="Password" class="form-control" name="password">
                            </div>
                            <button class="btn btn-sm btn-purple btn-block" type="submit">
                                <strong>Login</strong>
                            </button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    @endif
@endsection