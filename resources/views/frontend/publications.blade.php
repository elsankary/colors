@extends('frontend.layouts.default')

@section('content')
    <section>
        <div id="cover" style="background-image: url({{ asset('assets/frontend/img/publications.jpg') }});">
            <div class="overla">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12 m-t-160">
                            <h1 class="white-color text-center bold font-42">Publications</h1>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section id="event">
        <div class="container m-b-40 m-t-20">
            <div>
                <h2 class="main-color font-30 bold">Our Publications</h2>
            </div>
            <div class="row ">
                @foreach($articles as $article)
                    <div class="col-md-5 m-t-20">
                        <div class="block">
                            <h3 class="bold font-22">{{ $article->title }}</h3>
                            <div class="row">
                                <div class="col-md-12">
                                    <p class="regular">{{ str_limit(strip_tags($article->body), 250) }}</p>
                                </div>
                                <div class="col-md-12">
                                    <a href="{{ url('articles', $article->id) }}" type="button" class="btn btn-Lemonade pull-right">More</a>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </section>

    @include('frontend.layouts.partials._footer')
@endsection