@extends('frontend.layouts.default')

@section('content')
    <section>
        <div id="cover" style="background-image: url({{ asset('assets/frontend/img/research.jpg') }});">
            <div class="overla">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12 m-t-160 text-center white-color">
                            <h1>Research Department</h1>
                            <button type="button" style="  font-family: MyriadPro-Light;    padding: 4px 30px;     font-size: 22px;
                        " class="btn btn-yollow m-t-10 btn-request">
                                Request
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section id="category">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="tabs-container">
                        <div class="nav nav-tabs block text-center">
                            @foreach($tree as $x => $item)
                                <a data-toggle="tab" href="#tab-{{ $item->id }}"
                                   class="btn btn-danger btn-rounded btn-outline category research-item {{ $x == 0 ? 'active' : '' }}"
                                   data-section-id="{{ $item->id }}"
                                   data-active="{{ $x == 0 ? 1 : 0 }}"
                                >{{ $item->name }}</a>
                            @endforeach
                        </div>
                        <div class="tab-content text-center">
                            @foreach($tree as $item)
                                <div id="tab-{{ $item->id }}" class="tab-pane">
                                    <div class="panel-body">
                                        @foreach($item->children as $subItem)
                                            <a class="btn btn-danger btn-rounded btn-outline suncat research-item"
                                               data-section-id="{{ $subItem->id }}"
                                               data-active="0"
                                            >{{ $subItem->name }}</a>
                                        @endforeach
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    @include('frontend.research._modal')
@endsection

@section('js')
    <script>
        $('.research-item').on('click', function () {
            $('.research-item').attr('data-active', 0).removeClass('active');
            $(this).attr('data-active', 1).addClass('active');
        });

        $('.btn-request').on('click', function () {
            var $modal = $('#request');
            $modal.find('input[name=section_id]').val($('.research-item[data-active=1]').data('section-id'));
            $modal.modal('show');
        });
    </script>
@append